" Vim syntax file
" Language: i3wm config file

" Quit when a (custom) syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

runtime syntax/conf.vim

syn keyword i3Command   bindsym bindcode exec gaps exec_always floating_modifier assign for_window
syn match   i3Command   '^\s*\%(set\>\|font\>\|mode\>\|bar\>\)'
syn match   i3Mod       '\s\+\$\w\+\>'

hi def link i3Command   Keyword
hi def link i3Mod       PreProc

let b:current_syntax = "i3"
