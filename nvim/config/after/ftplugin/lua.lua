local o = vim.opt_local
local tab = 3
o.suffixesadd:prepend('.lua')
-- vim.opt_local.suffixesadd:prepend('init.lua')
o.path:prepend(vim.fn.stdpath('config')..'/lua')
o.expandtab   = false -- true = convert tabs to spaces
o.shiftwidth  = tab
o.softtabstop = tab
o.tabstop     = tab
