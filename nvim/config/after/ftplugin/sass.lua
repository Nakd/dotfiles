local o = vim.opt_local
local tab = 4
o.expandtab   = false -- true = convert tabs to spaces
o.shiftwidth  = tab
o.softtabstop = tab
o.tabstop     = tab
