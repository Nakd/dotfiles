require("variables")
require("options")
require("theme")
require("statusline")
require("keys")
require("autocmds")
require("plugins")
require("swap")
-- require("statusline-again")
-- vim.opt.statusline = "%l:%c %= lua config/%t %L %=Nor g"
-- TODO show when lang server is loading?
-- TODO completion
-- TODO custom commands?
-- TODO dashboard or alpha
-- TODO optimize statusline?
