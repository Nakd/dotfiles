call plug#begin('~/.local/share/nvim/plugs')

" rainbow

" Plug 'neovim/nvim-lspconfig'              " lsp
" Plug 'nvim-lua/plenary.nvim'              " needed for null-ls
" Plug 'jose-elias-alvarez/null-ls.nvim'    " lsp?
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Plug 'nvim-treesitter/nvim-treesitter'      " highlighting
Plug 'hrsh7th/nvim-cmp'                     " lsp and autopairs
Plug 'tpope/vim-surround'
Plug 'windwp/nvim-autopairs'
Plug 'tpope/vim-commentary'                 " commenting
Plug 'norcalli/nvim-colorizer.lua'          " colors hex codes
Plug 'mhinz/vim-startify'
Plug 'bling/vim-bufferline'                 " buffer names in command line
Plug 'lukas-reineke/indent-blankline.nvim'  " vertical lines for indents
" Plug 'rktjmp/lush.nvim'                     " color scheme creator
" Plug 'ms-jpq/coq_nvim'

call plug#end()
