set breakindent                 " indented wrapping
set cursorline
set foldmethod=marker
set hidden                      " allow swapping from modififed buffer
set ignorecase                  " case-insensitive searching
set smartcase                   " case-sensitive if search contains uppercase letters
set linebreak                   " don't break words
set mouse=nvi                   " enable mouse use
set nobackup
set nohlsearch                  " don't highlight every match
set noshowmode                  " don't print mode in bottom line
set noswapfile
set nowritebackup
set nowrap
set nrformats+=alpha
set number                      " line numbers
set relativenumber
set scrolloff=99                " vertical cursor padding
set sidescrolloff=8             " horizontal cursor padding
set shiftwidth=4
set softtabstop=4               " makes tabs 4 spaces
set tabstop=4                   " makes tabs 4 spaces
set expandtab
set termguicolors               " enables 24-bit color
set title                       " sets window title
set undodir=~/.cache/nvim/undo  " persistent undo
set undofile                    " ^
set fillchars=eob:\
let &fcs='eob: '                " remove ~ from blank lines
set shell=powershell " windows shell
set shellquote="
set shellpipe=\|
set shellxquote=
set shellcmdflag=-NoLogo\ -NoProfile\ -ExecutionPolicy\ RemoteSigned\ -Command
set shellredir=\|\ Out-File\ -Encoding\ UTF8
:filetype on
:syntax on
