" set tabline=%Ttest%X
" set showtabline=1
set laststatus=2
set statusline=
set statusline+=%#mColor#\ %{SLcolor()}\ %#mColorA#    " mode 
set statusline+=%#NormalNC#\ %t\ %#ModeMsg#%m           " filename, modified
set statusline+=%=%#mColorA#%#mColor#\ %c\ %l/%L\      " lines 

function! SLcolor()
    if (mode()== 'n')
        exe 'hi mColor guibg=none guifg=none'
        exe 'hi mColorA guibg=none guifg=#1b1b1b'
        return "N"
    elseif (mode()== 'c')
        exe 'hi mColor guibg=#d1d1d1 guifg=#202020'
        exe 'hi mColorA guibg=none guifg=#d1d1d1'
        return "C"
    elseif (mode()== 'i')
        exe 'hi mColor guibg=#a1b56c guifg=#202020'
        exe 'hi mColorA guibg=none guifg=#a1b56c'
        return "I"
    elseif (mode()== 'v')
        exe 'hi mColor guibg=#65AAC3 guifg=#202020'
        exe 'hi mColorA guibg=none guifg=#65AAC3 '
        return "V"
    elseif (mode()== 'R')
        exe 'hi mColor guibg=#B84B47 guifg=#202020'
        exe 'hi mColorA guibg=none guifg=#B84B47 '
        return "R"
    endif
endfunction

" let g:mode={
"       \ 'n'  : 'Normal ',
"       \ 'no' : 'N·Operator Pending ',
"       \ 'v'  : 'Visual ',
"       \ 'V'  : 'V·Line ',
"       \ 'x22' : 'V·Block ',
"       \ 's'  : 'Select ',
"       \ 'S'  : 'S·Line ',
"       \ 'x19' : 'S·Block ',
"       \ 'i'  : 'Insert ',
"       \ 'R'  : 'Replace ',
"       \ 'Rv' : 'V·Replace ',
"       \ 'c'  : 'Command ',
"       \ 'cv' : 'Vim Ex ',
"       \ 'ce' : 'Ex ',
"       \ 'r'  : 'Prompt ',
"       \ 'rm' : 'More ',
"       \ 'r?' : 'Confirm ',
"       \ '!'  : 'Shell ',
"       \ 't'  : 'Terminal '
"       \}

