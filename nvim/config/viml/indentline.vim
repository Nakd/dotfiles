set colorcolumn=9999
let g:indent_blankline_filetype_exclude = ['help', 'text', 'man', 'startify', 'conf', 'packer']
