autocmd BufEnter * silent! lcd %:p:h " auto cd to file location
autocmd BufRead,BufNewFile   *.txt setlocal wrap linebreak

augroup Mkdir
    autocmd!
    autocmd BufWritePre * call mkdir(expand("<afile>:p:h"), "p")
augroup END

augroup syntax
    autocmd!
    autocmd BufRead,BufNewFile *.fish set ft=sh " why is ft needed?
    autocmd BufRead,BufNewFile *.rasi setf css
    autocmd BufRead,BufNewFile *.yml setf yaml
    autocmd BufRead,BufNewFile *.glsl setf c
    autocmd BufRead,BufNewFile *.conf setf conf
    autocmd BufRead,BufNewFile *.theme setf dosini
    autocmd BufRead,BufNewFile *.vifm setf vim
augroup END

" enable cursorline for Startify
autocmd User Startified setlocal cursorline
