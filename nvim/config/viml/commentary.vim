" default comment text
" set commentstring=#\ %s

" use gc instead of gcc
if !hasmapto('<Plug>Commentary') " || maparg('gc','n') ==# ''
  xmap gc  <Plug>Commentary
  nmap gc  <Plug>Commentary
  omap gc  <Plug>Commentary
  nmap gc  <Plug>CommentaryLine
  " if maparg('c','n') ==# '' && !exists('v:operator')
  "   nmap cgc <Plug>ChangeCommentary
  " endif
  " nmap gcu <Plug>Commentary<Plug>Commentary
endif
