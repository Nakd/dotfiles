" make space do nothing
" noremap <space> <nop>

" set leader key to space
" let mapleader = " "

" nnoremap <c-s> :update<CR>
" vnoremap <c-s> <ESC>:update<CR>v
" inoremap <c-s> <ESC>:update<CR>i

" buffer navigation
" nnoremap <silent> <A-h> :bp<CR>
" nnoremap <silent> <A-l> :bn<CR>
" inoremap <silent> <A-h> <ESC>:bp<CR>
" inoremap <silent> <A-l> <ESC>:bn<CR>
" vnoremap <silent> <A-h> <ESC>:bp<CR>
" vnoremap <silent> <A-l> <ESC>:bn<CR>
" nnoremap <silent> <A-q> :bd<CR>

" split navigation
nnoremap <silent> <A-j> <C-w>j
nnoremap <silent> <A-k> <C-w>k

" open file in clipboard?
" noremap <silent> <C-S-o> <esc>:e<space><C-r>*<enter>
" noremap! <silent> <C-S-o> <esc>:e<space><C-r>*<enter>

" copy/paste
" vnoremap <leader>y "+y1v
" vnoremap <leader>p "+pv

" print current directory
" noremap <leader>d :pwd<CR>

" visual block mode
" noremap <leader>v <C-v>

" quit
" noremap <leader>q :q<CR>

" turn on wrapping
" noremap <leader>w :set wrap<CR>

" capslock = F6
" noremap <F6> <esc>
" inoremap <F6> <esc>
" cnoremap <F6> <C-c>

" global replace
" nnoremap <leader>r :%s//g<left><left>

" toggle true / false
" nnoremap <leader>t ciwtrue<ESC>
" nnoremap <leader>f ciwfalse<ESC>
