local api = vim.api
local fn = vim.fn
local diagnostic = vim.diagnostic

local function getmode()
	return api.nvim_get_mode().mode
end

local function width(item)
	return api.nvim_eval_statusline(item, {winid = 0})["width"]
end

local modes = setmetatable({
	["n"]  = {text = {"Nor", "N"}, color = {"%#StatusN#", "%#StatusN2#"}},
	["i"]  = {text = {"Ins", "I"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["ix"] = {text = {"Cmp", "I"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["ic"] = {text = {"Add", "A"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["c"]  = {text = {"Cmd", "C"}, color = {"%#StatusC#", "%#StatusC2#"}},
	["R"]  = {text = {"Rep", "R"}, color = {"%#StatusR#", "%#StatusR2#"}},
	["Rv"] = {text = {"R-V", "R"}, color = {"%#StatusR#", "%#StatusR2#"}},
	["v"]  = {text = {"Vis", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	["V"]  = {text = {"V-L", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	[""] = {text = {"V-B", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	["t"]  = {text = {"Ter", "T"}, color = {"%#StatusT#", "%#StatusT2#"}},
	["nt"] = {text = {"T-N", "T"}, color = {"%#StatusT#", "%#StatusT2#"}},
	["r?"] = {text = {"Y/N", "?"}, color = {"%#StatusQ#", "%#StatusQ2#"}},
	["s"]  = {text = {"Sel", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
	["S"]  = {text = {"S-L", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
	[""] = {text = {"S-B", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
}, {
	__index = function() -- handle unknown modes
		return {text = {getmode(), "!"}, color = {"%#StatusUnknown#", "%#StatusUnknown2#"}}
	end
})

local color = {
	mode = {},
	norm = "%#Normal#",
	mod  = "%#Parameter#",
	dim  = "%#Comment#",
	dir  = "%#StatusDir#",
	buf  = "%#Include#",
	search = "%#StatusSearch#"
}

local deco = { -- ICONS:        │┊┆
	tr    = "",
	bl    = "",
	br    = "",
	tl    = "",
	read  = "",
	nomod = ""
}

local align = "%#Bracket#%="
local home  = fn.expand("$HOME")
local spc   = " "
local trim  = 8

local lines    = {text = color.dim .. " %L"}
local backup   = {text = ""}
local buf      = {text = ""}
local diag     = {text = ""}
local filename = {text = ""}
local fileflag = {text = ""}
local filetype = {text = ""}
local m_text   = {text = ""}
local pad_l    = {text = ""}
local pad_r    = {text = ""}
local pos      = {text = ""}
local search   = {text = ""}

local mode
	local list = { -- trim order
		pad_l,
		pad_r,
		backup,
		m_text,
		filetype,
		lines,
		search,
		pos,
		-- f_mod,
		-- don't trim below --
		filename,
		fileflag,
		buf,
		diag,
	}
local LEFT
local MID
local RIGHT

local function format()

	pad_l.text = ""
	pad_r.text = ""
	for i=1, #list do -- get width for each part
		list[i].width = width(list[i].text)
	end

	for i in pairs(LEFT.items) do -- get LEFT width
		LEFT.width = LEFT.width + LEFT.items[i].width
	end

	for i in pairs(MID.items) do -- get MID width
		MID.width = MID.width + MID.items[i].width
	end

	for i in pairs(RIGHT.items) do -- get RIGHT width
		RIGHT.width = RIGHT.width + RIGHT.items[i].width
	end

	-- add padding
	local diff = LEFT.width - RIGHT.width
	if diff > 0 then -- LEFT is wider
		diff        = math.abs(diff)
		pad_r.text  = string.rep(spc, diff)
		pad_r.width = diff
		RIGHT.width = RIGHT.width + diff
	elseif diff < 0 then -- RIGHT is wider
		diff        = math.abs(diff)
		pad_l.text  = string.rep(spc, diff)
		pad_l.width = diff
		LEFT.width  = LEFT.width + diff
	end

	local status_w = LEFT.width + MID.width + RIGHT.width
	local window_w = api.nvim_win_get_width(0)
	-- print(LEFT.width, MID.width, RIGHT.width, status_w, window_w)

	if window_w < status_w then
		for i=1, trim do
			list[i].text = ""
			status_w = status_w - list[i].width
			if window_w >= status_w then break end
		end
	end
end

-- TODO somehow only update what needs to be updated? (autocmds)
-- cursormoved, modified, bufenter
function _Status()

	--       LAYOUT:
	LEFT = {
		items = {pos, buf, pad_l},
		text  = "",
		width = 0,
	}
	MID = {
		items = {fileflag, filetype, filename, lines},
		text  = "",
		width = 0,
	}
	RIGHT = {
		items = {pad_r, search, diag, m_text, backup},
		text  = "",
		width = 0,
	}

	mode          = getmode()
	color.mode[1] = modes[mode].color[1]
	color.mode[2] = modes[mode].color[2]

	-- ASSIGN_PARTS:
	pos.text = color.mode[1] .. " %l:%v "

	-- pos.text = color.mode[1] .. " %l:%c " .. color.mode[2] .. deco.bl
	-- TODO implement tabs?: current buf has all info,
	-- others just have name + modified indication

	buf.text = #fn.getbufinfo({buflisted = 1})
	if buf.text > 1 then
		-- buf.text = color.mode[2] .. deco.tr .. color.mode[1] .. buf.text .. color.mode[2] .. deco.bl
		-- buf.text = "%#StatusB2#" .. deco.tr .. "%#StatusB#" .. buf.text .. "%#StatusB2#" .. deco.bl
		buf.text = " " .. buf.text .. spc .. color.mode[2] .. deco.bl
	else
		buf.text = color.mode[2] .. deco.bl
	end

	if vim.bo.modified then
		color.file = "%#StatusMod#"
		color.dir = "%#StatusDir2#"
	else
		color.file = "%#Normal#"
		color.dir = "%#StatusDir#"
	end

	if not vim.bo.modifiable then
		fileflag.text = color.dim .. deco.nomod .. spc
	elseif vim.bo.readonly then
		fileflag.text = color.dim .. deco.read .. spc
	else
		fileflag.text = ""
	end

	filename.text = ""
	-- for i in string.gmatch(api.nvim_eval_statusline("%F", {winid = 0})["str"], "[^/]+/") do
	-- 	filename.text = color.dir .. i
	-- end
	filename.text = filename.text .. color.file .. "%t"

	local type = api.nvim_buf_get_option(0, "filetype")
	if type:len() > 0 then
		filetype.text = color.dim .. type .. spc
	else
		filetype.text = color.dim
	end

	search.text = fn.searchcount({maxcount = 0})
	if search.text.total == 0 then
		search.text = ""
	else
		search.text = color.search .. search.text.current .. "/" .. search.text.total .. spc
	end
	-- search.text = "" .. search.text.count .. spc
	-- search.p = color.norm .. " " .. color.dim .. fn.getreg("/") .. color.norm .. " " .. search.p.count

	m_text.text = color.mode[2] .. deco.br .. color.mode[1] .. spc .. modes[mode].text[1] .. spc
	if api.nvim_buf_get_name(0):match("^" .. home .. "/dots") then
		backup.text = color.mode[1] .. " "
	else
		backup.text = ""
	end

	diag.text = ""
	diag.error = #diagnostic.get(0, { severity = diagnostic.severity.ERROR })
	if diag.error > 0 then
		diag.text = diag.text .. "%#StatusDiagE#" .. deco.tl .. diag.error .. deco.br
	end
	diag.warn = #diagnostic.get(0, { severity = diagnostic.severity.WARN })
	if diag.warn > 0 then
		diag.text = diag.text .. "%#StatusDiagW#" .. deco.tl .. diag.warn .. deco.br
	end
	diag.info = #diagnostic.get(0, { severity = diagnostic.severity.INFO })
	if diag.info > 0 then
		diag.text = diag.text .. "%#StatusDiagI#" .. deco.tl .. diag.info .. deco.br
	end
	diag.hint = #diagnostic.get(0, { severity = diagnostic.severity.HINT })
	if diag.hint > 0 then
		diag.text = diag.text .. "%#StatusDiagH#" .. deco.tl .. diag.hint .. deco.br
	end

	format()

	-- COMBINE_PARTS:
	for i in pairs(LEFT.items) do
		LEFT.text = LEFT.text .. LEFT.items[i].text end
	for i in pairs(MID.items) do
		MID.text = MID.text .. MID.items[i].text end
	for i in pairs(RIGHT.items) do
		RIGHT.text = RIGHT.text .. RIGHT.items[i].text end

	-- OUTPUT:
	-- print(LEFT.text..align..MID.text..align..RIGHT.text)
	return LEFT.text .. align .. MID.text .. align .. RIGHT.text
end
vim.opt.statusline = "%!v:lua._Status()"
