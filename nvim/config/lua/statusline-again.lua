-- INIT_START:
-- set vars and funcs
-- set text for each item using funcs
-- padding
-- set statusline
-- INIT_END:
-- LOOP:
-- on events, update relevant items
-- check width of relevant items to determine if padding should be changed

local api = vim.api
local fn = vim.fn
local opt = vim.opt

local function getmode()
	return api.nvim_get_mode().mode
end

local function width(item)
	return api.nvim_eval_statusline(item, {winid = 0})["width"]
end

local modes = setmetatable({--{{{
	["n"]  = {text = {"Nor", "N"}, color = {"%#StatusN#", "%#StatusN2#"}},
	["i"]  = {text = {"Ins", "I"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["ix"] = {text = {"Cmp", "I"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["ic"] = {text = {"Add", "A"}, color = {"%#StatusI#", "%#StatusI2#"}},
	["c"]  = {text = {"Com", "C"}, color = {"%#StatusC#", "%#StatusC2#"}},
	["R"]  = {text = {"Rep", "R"}, color = {"%#StatusR#", "%#StatusR2#"}},
	["v"]  = {text = {"Vis", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	["V"]  = {text = {"V-L", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	[""] = {text = {"V-B", "V"}, color = {"%#StatusV#", "%#StatusV2#"}},
	["t"]  = {text = {"Ter", "T"}, color = {"%#StatusT#", "%#StatusT2#"}},
	["nt"] = {text = {"T-N", "T"}, color = {"%#StatusT#", "%#StatusT2#"}},
	["r?"] = {text = {"Y/N", "?"}, color = {"%#StatusQ#", "%#StatusQ2#"}},
	["s"]  = {text = {"Sel", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
	["S"]  = {text = {"S-L", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
	[""] = {text = {"S-B", "S"}, color = {"%#StatusS#", "%#StatusS2#"}},
}, {
	__index = function() -- handle unknown modes
		return {text = {getmode(), "!"}, color = {"%#StatusUnknown#", "%#StatusUnknown2#"}}
	end
})--}}}

local color = {
	dim    = "%#Comment#",
	dim2   = "%#StatusDir#",
	file   = "%#Normal#",
	mode   = {},
	norm   = "%#Normal#",
	search = "%#StatusSearch#",
}

local deco = {
	bl = "",
	br = "",
	tl = "",
	tr = "",
}

-- ICONS:
-- 
-- │┊┆
--       

local mode
local spc      = " "
local align    = "%#Bracket#%="
local lines    = {text = color.dim .. " %L "}
local pad_l    = {text = ""}
local pad_r    = {text = ""}
local backup   = {text = ""}
local buf      = {text = ""}
local diag     = {text = ""}
local fileflag = {text = ""}
local filename = {text = ""}
local filetype = {text = ""}
local m_text   = {text = ""}
local pos      = {text = ""}
local search   = {text = ""}

-- LAYOUT:
local Left = {
	items = {pos, buf, pad_l},
	text = "",
	width = 0,
}
local Mid = {
	items = {fileflag, filetype, filename, lines},
	text = "",
	width = 0,
}
local Right = {
	items = {pad_r, search, diag, m_text, backup},
	text = "",
	width = 0,
}

local list = { -- trim order
	trim = {
		pad_l,
		pad_r,
		backup,
		m_text,
		filetype,
		lines,
		search,
		pos,
	},
	keep = {
		-- f_mod,
		filename,
		fileflag,
		buf,
		diag,
	},
}

-- local events = {
--   "FileType",
--   "BufWinEnter",
--   "BufReadPost",
--   "BufWritePost",
--   "BufEnter",
--   "WinEnter",
--   "FileChangedShellPost",
--   "VimResized",
--   "TermOpen",
-- }

local function padding()
	local diff = Left.width - Right.width
	print(Left.width, Mid.width, Right.width, diff)
	-- print(Left.width, Mid.width, Right.width, pad_l.width, pad_r.width)
	-- print(diff)
	if diff > 0 then -- left is wider
		diff        = math.abs(diff)
		pad_r.text  = string.rep("@", diff)
		pad_r.width = diff
		Right.width = Right.width + diff
	elseif diff < 0 then -- right is wider
		diff        = math.abs(diff)
		pad_l.text  = string.rep("@", diff)
		pad_l.width = diff
		Left.width  = Left.width + diff
	end
end

local function format()--{{{
	-- TODO if width of item ~= last width, then format?
	-- Left.text   = ""
	Left.width  = 0
	-- Mid.text    = ""
	Mid.width   = 0
	-- Right.text  = ""
	Right.width = 0

	pad_l.text = ""
	pad_r.text = ""

	for i=1, #list.trim do -- get width for each part in trim list
		list.trim[i].width = width(list.trim[i].text)
	end
	for i=1, #list.keep do -- get width for each part in keep list
		list.keep[i].width = width(list.keep[i].text)
	end

	for _, item in pairs(Left.items) do -- get left width
		Left.width = Left.width + item.width
	end

	for _, item in pairs(Mid.items) do -- get mid width
		Mid.width = Mid.width + item.width
	end

	for _, item in pairs(Right.items) do -- get right width
		Right.width = Right.width + item.width
	end

	padding()
	-- print(Left.width, Mid.width, Right.width, pad_l.width, pad_r.width)

	local status_w = Left.width + Mid.width + Right.width
	local window_w = api.nvim_win_get_width(0)
	-- print(window_w, status_w)

	if window_w < status_w then
		for i=1, #list.trim do
			list.trim[i].text = ""
			status_w = status_w - list.trim[i].width
			if window_w >= status_w then break end
		end
	end
	opt.statusline = Left.text .. align .. Mid.text .. align .. Right.text
end
--}}}

local function combine()
	-- COMBINE_PARTS:
	Left.text = ""
	for i in pairs(Left.items) do
		Left.text = Left.text .. Left.items[i].text
	end
	Mid.text = ""
	for i in pairs(Mid.items) do
		Mid.text = Mid.text .. Mid.items[i].text
	end
	-- print(">"..lines.text.."<")
	Right.text = ""
	for i in pairs(Right.items) do
		Right.text = Right.text .. Right.items[i].text
	end
end

local function f_mode()
	mode          = getmode()
	color.mode[1] = modes[mode].color[1]
	color.mode[2] = modes[mode].color[2]
	pos.text = color.mode[1] .. " %l:%c "
	m_text.text = color.mode[2] .. deco.br .. color.mode[1] .. spc .. modes[mode].text[1] .. spc
end

local function f_filename()
	filename.dir = ""
	for i in string.gmatch(api.nvim_eval_statusline("%F", {winid = 0})["str"], "[^/]*/") do
		filename.dir = color.dim2 .. i
	end
	if vim.bo.modified then
		color.file = "%#StatusMod#"
	else
		color.file = "%#Normal#"
	end
	filename.text = spc .. filename.dir .. color.file .. "%t"
end

local function f_filetype()
	local type = api.nvim_buf_get_option(0, "filetype")
	if type:len() > 0 then
		filetype.text = spc .. color.dim .. type
	else
		filetype.text = color.dim
	end
end
-- TODO register client to get progress messages?
-- (https://github.com/arkav/lualine-lsp-progress/blob/master/lua/lualine/components/lsp_progress.lua)
local function f_diag()
	diag.text = ""
	diag.error = #vim.diagnostic.get(0, {severity = vim.diagnostic.severity.ERROR})
	if diag.error > 0 then
		diag.text = diag.text .. "%#StatusDiagE#" .. diag.error .. ""
	end
	diag.warn = #vim.diagnostic.get(0, {severity = vim.diagnostic.severity.WARN})
	if diag.warn > 0 then
		diag.text = diag.text .. "%#StatusDiagW#" .. diag.warn .. ""
	end
	diag.info = #vim.diagnostic.get(0, {severity = vim.diagnostic.severity.INFO})
	if diag.info > 0 then
		diag.text = diag.text .. "%#StatusDiagI#" .. diag.info .. ""
	end
	diag.hint = #vim.diagnostic.get(0, {severity = vim.diagnostic.severity.HINT})
	if diag.hint > 0 then
		diag.text = diag.text .. "%#StatusDiagH#" .. diag.hint .. ""
	end
end

-- local function f_search()
-- 	search.text = fn.searchcount({maxcount = 0})
-- 	if search.text.total == 0 then
-- 		search.text.count = ""
-- 	else
-- 		search.text.count = color.norm .. search.text.current .. "/" .. search.text.total .. spc
-- 	end
-- 	search.text = "" .. search.text.count .. ""
-- end

local function f_buf()
	buf.text = #fn.getbufinfo({buflisted = 1})
	if buf.text > 1 then
		buf.text = " " .. buf.text .. spc .. color.mode[2] .. deco.bl
	else
		buf.text = color.mode[2] .. deco.bl
	end
end

local function f_fileflag()
	if not vim.bo.modifiable then
		fileflag.text = color.dim .. " "
	elseif vim.bo.readonly then
		fileflag.text = color.dim .. " "
	else
		fileflag.text = ""
	end
end

local home = fn.expand("~")
local function f_backup()
	if api.nvim_buf_get_name(0):match("^" .. home .. "/dots") then
		backup.text = color.mode[1] .. " "
	else
		backup.text = ""
	end
end

local M = {}
function M.ModeChanged()
	-- print("change " .. math.random(99))
	f_mode()
	f_diag()
	f_buf()
	f_backup()
	combine()
	format()
	if mode == "c" or mode == "r?" then
		vim.cmd("redrawstatus")
	end
end

-- function M.BufModifiedSet()
-- 	f_filename()
-- 	combine()
-- 	format()
-- end
--
-- function M.BufEnter()
-- 	f_mode()
-- 	f_filename()
-- 	f_fileflag()
-- 	f_filetype()
-- 	f_buf()
-- 	f_backup()
-- 	combine()
-- 	format()
-- end
--
-- function M.TextChanged()
-- 	-- combine()
-- 	-- format()
-- end

function M.Update()
	f_mode()
	f_filename()
	f_fileflag()
	f_filetype()
	f_buf()
	f_backup()
	f_diag()

	combine()
	format()
	-- if mode == "c" or mode == "r?" then
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		-- fuck you, you piece of fucking garbage
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
		vim.cmd("redrawstatus")
	-- end
	-- print(mode)
end

-- get width1, width2
-- if width2 > width1, Left.width - (w1 - w2)
-- if width2 < width1, Left.width - (w1 - w2)
-- local w1 = 0
-- function M.CursorMoved()
-- 	M.Update()
-- 	-- local w2 = width(Left.text)
-- 	-- print(w1, w2)
-- 	-- if w1 ~= w2 then
-- 	-- 	Left.width = Left.width - (w1 - w2)
-- 	-- 	w1 = w2
-- 	-- 	padding()
-- 	-- 	combine()
-- 	-- 	opt.statusline = Left.text .. align .. Mid.text .. align .. Right.text
-- 	-- 	-- format()
-- 	-- end
-- 	-- print(w1, w2, Left.width)
-- end

f_mode()
f_buf()
f_fileflag()
f_filetype()
f_filename()
f_backup()

combine()
format()

return M
