-- TODO rewrite this crap
-- TODO optimize (use autocmds to update certain parts?), move stuff to tables
-- TODO break up width checks

-- MODES:
local function getmode()
	return vim.api.nvim_get_mode().mode
end

local modes = setmetatable({
	["n"]  = {text = "Nor", text2 = "N", color = "%#StatusN#", color2 = "%#StatusN2#"},
	["i"]  = {text = "Ins", text2 = "I", color = "%#StatusI#", color2 = "%#StatusI2#"},
	["ix"] = {text = "Cmp", text2 = "I", color = "%#StatusI#", color2 = "%#StatusI2#"},
	["ic"] = {text = "Add", text2 = "A", color = "%#StatusI#", color2 = "%#StatusI2#"},
	["c"]  = {text = "Com", text2 = "C", color = "%#StatusC#", color2 = "%#StatusC2#"},
	["R"]  = {text = "Rep", text2 = "R", color = "%#StatusR#", color2 = "%#StatusR2#"},
	["v"]  = {text = "Vis", text2 = "V", color = "%#StatusV#", color2 = "%#StatusV2#"},
	["V"]  = {text = "V-L", text2 = "V", color = "%#StatusV#", color2 = "%#StatusV2#"},
	[""] = {text = "V-B", text2 = "V", color = "%#StatusV#", color2 = "%#StatusV2#"},
	["t"]  = {text = "Ter", text2 = "T", color = "%#StatusT#", color2 = "%#StatusT2#"},
	["nt"] = {text = "T-N", text2 = "T", color = "%#StatusT#", color2 = "%#StatusT2#"},
	["r?"] = {text = "Y/N", text2 = "?", color = "%#StatusQ#", color2 = "%#StatusQ2#"},
	["s"]  = {text = "Sel", text2 = "S", color = "%#StatusS#", color2 = "%#StatusS2#"},
	["S"]  = {text = "S-L", text2 = "S", color = "%#StatusS#", color2 = "%#StatusS2#"},
	[""] = {text = "S-B", text2 = "S", color = "%#StatusS#", color2 = "%#StatusS2#"},
}, {
	__index = function() -- handle unknown modes
		return {text = " " .. getmode() .. " ", color = "%#StatusUnknown#", color2 = "%#StatusUnknown2#"}
	end
})

-- VARIABLES:
-- local dir =
-- local dir_long = vim.api.nvim_eval_statusline("%F", {winid = 0})["str"]
-- local file_len = vim.api.nvim_eval_statusline("%t", {winid = 0})["width"]
local p
local align = "%="
local backup
local buf
local diff
local file_name
local file_type
local left
local pos
local line_total
local middle
local mode
local file_prefix
local pad_l = ""
local pad_r = ""
local right
local text_mode
local color = {
	file     = "%#Normal#",
	modified = "%#parameter#",
	dim      = "%#Comment#",
}
local width = {}
local deco = {}

local function length(item)
	return vim.api.nvim_eval_statusline(item, {winid = 0})["width"]
end

local function check_width()
	width = {
		l = length(left),
		m = length(middle),
		r = length(right),
		w = vim.api.nvim_win_get_width(0)
	}
	width.s = width.l + width.m + width.r
	width.diff_s = width.l - width.r
	width.diff_w = width.w - width.s - #pad_l - #pad_r
	-- os.execute("dunstify -t 2000 -r 1 'left:   "..width.l.."\nright:  "..width.r.."\ndiff_s: "..width.diff_s.."'")
end

local function combine(pad)
	if not pad then
		pad_l = ""
		pad_r = ""
	end
	left =
		pos..
		buf..
		pad_l..
		""
	middle =
		mod_l..
		file_prefix..
		file_name..
		file_type..
		line_total..
		mod_r..
		""
	right =
		pad_r..
		search..
		text_mode..
		backup..
		""
end

function Status()

	deco = { -- ICONS:        │┊┆
		l1 = "",
		l2 = "",
		r1 = "",
		r2 = "",
	}

	file_prefix  = ""
	line_total   = " %L"
	mode         = getmode()
	color.mode   = modes[mode].color
	color.mode_2 = modes[mode].color2
	file_type    = vim.api.nvim_buf_get_option(0, "filetype")
	buf          = fn.len(fn.getbufinfo({buflisted = 1}))
	file_name    = color.file.."%t" -- TODO dir/file
	search       = fn.searchcount({maxcount = 0})

	if search.total == 0 then
		search = "%#comment#"..""..search.current.."/"..search.total.." "
	-- elseif search.current == search.total then
	-- 	search = "%#builtin#"..""..search.current.."/"..search.total.." "
	-- elseif search.current == 1 then
	-- 	search = "%#string#"..""..search.current.."/"..search.total.." "
	else
		search = "%#normal#"..""..search.current.."/"..search.total.." "
	end

	pos =
		color.mode..
		" %lL "..
		"%cC "..
		color.mode_2..
		deco.l2..
		""

	text_mode =
		color.mode_2..
		deco.r1..
		color.mode..
		" "..
		modes[mode].text..
		" "..
		""

	if file_type:len() == 0 then
		file_type = ""
	else
		file_type = color.dim.." "..file_type
	end

	if vim.bo.modified == true then
		mod_l = color.modified.."◆ "
		mod_r = "  "
		color.file = "%#StatusMod#"
	else
		mod_l = "  "
		mod_r = "  "
		color.file = "%#Normal#"
	end

	if vim.bo.modifiable == false then
		file_prefix = color.dim.." "
	elseif vim.bo.readonly == true then
		file_prefix = color.dim.." "
	end

	if buf > 1 then
		buf =
			-- "%#StatusN2#"..
			-- color.mode_2..
			-- ""..
			-- color.mode..
			"%#delimiter#"..
			" "..buf..
			-- "%#StatusN2#"..
			-- color.mode_2..
			-- ""..
			"%#Normal#"..
			""
		-- buf = "" .. buf .. " │ "
	else
		buf = ""
	end

	if vim.api.nvim_buf_get_name(0):match("^/home/nakd/dots") then
		backup = color.mode.." "
	else
		backup = ""
	end

	-- p = {
	-- 	pad_l      = pad_l,
	-- 	pad_r      = pad_r,
	-- 	mod_r      = mod_r,
	-- 	pos        = pos,
	-- 	file_type  = file_type,
	-- 	line_total = line_total,
	-- 	text_mode  = text_mode,
	-- 	mod_l      = mod_l,
	-- 	backup     = backup,
	-- }

	combine()
	check_width()

	-- print(p.file_type)
	-- diff = width.w - width.s
	-- if diff < 0 then -- window smaller than statusline
	-- 	local list = { -- parts
	-- 		pad_l,
	-- 		pad_r,
	-- 		mod_r,
	-- 		pos,
	-- 		file_type,
	-- 		line_total,
	-- 		text_mode,
	-- 		mod_l,
	-- 		backup,
	-- 	}
	--
	-- 	-- refer to items in combine() as p.name
	-- 	-- maintain order
	-- 	-- set item in list to "",
	-- 	-- if new diff is >= 0 then break,
	-- 	-- else continue
	-- 	-- print(diff)
	-- 	color.file = "%#conditional#"
	-- 	for _, v in ipairs(list) do
	-- 		p.v = ""
	--
	-- 		-- print(p.pad_l)
	-- 		-- for k, v in pairs(p) do
	-- 		-- 	print()
	-- 		-- end
	-- 	end
	-- end

	-- TODO better way to do this? (break parts into table > iterate)
	diff = width.w - width.s
	if diff < 0 then -- window smaller than statusline
		local list = { -- parts
			"pad_l",
			"pad_r",
			"mod_r",
			"pos",
			"file_type",
			"line_total",
			"text_mode",
			"mod_l",
			"backup",
		}
		-- print(diff)
		-- diff = diff + #pad_l + #pad_r
		color.file = "%#conditional#"
		-- pad_l = ""
		-- pad_r = ""
		for _, i in ipairs(list) do
			loadstring(i.." = ''")()
		end
		-- if diff < 0 then
		-- 	diff = diff + 2
		-- 	text_mode = " "..modes[mode].text2.." "
		-- 	if diff < 0 then
		-- 		diff = diff + length(file_type)
		-- 		file_type = ""
		-- 		if diff < 0 then
		-- 			diff = diff + length(line_total)
		-- 			line_total = ""
		-- 			-- if diff < 0 then
		-- 			-- 	diff = diff + length(char)
		-- 			-- 	char = ""
		-- 			if diff < 0 then
		-- 				diff = diff + length(right)
		-- 				deco.r1 = ""
		-- 				text_mode = ""
		-- 				if diff < 0 then
		-- 					diff = diff + length(pos)
		-- 					pos = ""
		-- 				end
		-- 			end
		-- 		end
		-- 	end
		-- end
	end

	-- center the middle section by adding spaces to left or right
	if width.diff_s > 0 then -- left is longer
		pad_r = string.rep(" ", math.abs(width.diff_s))
		-- pad_r = ""
		-- for i=1, math.abs(width.diff_s) do pad_r = pad_r.." " end
	elseif width.diff_s < 0 then -- right is longer
		pad_l = string.rep(" ", math.abs(width.diff_s))
		-- pad_l = ""
		-- for i=1, math.abs(width.diff_s) do pad_l= pad_l.." " end
	end
	-- print(pad_l.."|"..pad_r)

	combine("pad")
	check_width()
	-- print(width.s, width.w)

	return -- final output
		left..
		align..
		middle..
		align..
		right
end

-- function StatusActive() vim.wo.statusline = "%!v:lua.Status()" end
-- function StatusInactive() vim.wo.statusline = Status() end

o.statusline = "%!v:lua.Status()"
