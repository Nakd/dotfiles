cmd([[
    " auto run PackerSync after saving plugins.lua
    " augroup packer_config
    "     autocmd!
    "     autocmd BufWritePost plugins.lua source <afile> | PackerSync
    " augroup end

    " autocmd BufWritePost *.py :silent !black %
    autocmd VimResized * wincmd =
    autocmd BufWritePost ~/dots/kitty/kitty.conf :silent !kill -USR1 $(pgrep kitty)
    autocmd BufEnter * setlocal formatoptions-=o
    autocmd BufEnter * setlocal cursorline
    "autocmd BufLeave * setlocal nocursorline
    autocmd InsertEnter * setlocal nolist
    autocmd InsertLeave * setlocal list
    autocmd BufEnter * silent! lcd %:p:h " auto cd to file location
    autocmd BufRead,BufNewFile *.txt setlocal wrap linebreak
    " autocmd BufRead,BufNewFile *.md setlocal wrap linebreak
    autocmd BufRead,BufNewFile /tmp/* setlocal noundofile

    " augroup Mkdir
    "     autocmd!
    "     autocmd BufWritePre * call mkdir(expand("<afile>:p:h"), "p")
    " augroup END

    augroup syntax
        autocmd!
        autocmd BufRead,BufNewFile *.conf setf conf
        autocmd BufRead,BufNewFile *.glsl setf glsl
        autocmd BufRead,BufNewFile *.inf setf dosini
        autocmd BufRead,BufNewFile *.rasi set ft=rasi commentstring=/*%s*/
        autocmd BufRead,BufNewFile *.reg setf registry
        autocmd BufRead,BufNewFile *.rules setf udevrules
        autocmd BufRead,BufNewFile *.theme setf conf
        autocmd BufRead,BufNewFile *.vifm setf vim
        autocmd BufRead,BufNewFile *.yml setf yaml
        autocmd BufRead,BufNewFile fish_variables setf fish
        autocmd FileType i3config call timer_start(0, {tid -> execute('so ~/.config/nvim/lua/theme.lua')})
        "autocmd FileType i3config so ~/.config/nvim/lua/theme.lua
    augroup END

    " enable cursorline for Startify
    autocmd User Startified setlocal cursorline
]])
