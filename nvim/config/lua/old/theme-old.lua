local theme = {

    Bold               = {style = 'bold'},
    BoldItalic         = {style = 'bold,italic'},
    Boolean            = {fg = cB, bg =  x, style = 'italic'},
    Bracket            = {fg = cF, style = x},
    Builtin            = {fg = cD, bg =  x, style = x},
    Character          = {fg = c5, bg =  x, style = 'underline', sp = cB}, -- ?
    ColorColumn        = {fg = c5, bg =  x, style = x},
    Comment            = {fg = c3, bg =  x, style = x},
    Conceal            = {fg = c5, bg =  x, style = x},
    Conditional        = {fg = cA, bg =  x, style = x}, -- if, then, else, etc.
    Constant           = {fg = c9, bg =  x, style = x}, -- $ in fish and all caps var in sh
    Cursor             = {fg = c5, bg =  x, style = x},
    CursorColumn       = {link = 'cursorline'},
    CursorIM           = {fg = c5, bg =  x, style = x},
    CursorLine         = {fg =  x, bg = c1, style = x},
    CursorLineNr       = {fg = c5, bg = c1, style = x},
    DarkenedPanel      = {fg = c5, bg =  x, style = x},
    DarkenedStatusline = {fg = c5, bg =  x, style = x},
    Debug              = {fg = c5, bg =  x, style = 'underline', sp = c9}, -- ?
    Define             = {fg = c5, bg =  x, style = x},
    Delimiter          = {fg = cF, bg =  x, style = 'bold'},
    DiffAdd            = {fg = cB, bg =  x, style = x}, -- added line in diff mode
    DiffChange         = {fg = c5, bg =  x, style = x}, -- changed line in diff mode
    DiffDelete         = {fg = c8, bg =  x, style = x}, -- deleted text in diff mode
    DiffText           = {fg = cD, bg =  x, style = x}, -- changed text in diff mode
    Directory          = {fg = c5, bg =  x, style = x},
    EndOfBuffer        = {fg = c5, bg =  x, style = x},
    Error              = {link = 'TSError'},
    ErrorMsg           = {fg = c8, bg =  x, style = x}, -- error in command line
    Exception          = {fg = cA, bg =  x, style = 'italic'}, -- try, catch, etc.
    Field              = {fg = c5, style = 'bold'},
    Float              = {link = 'Number'},
    FloatBorder        = {fg = c5, bg =  x, style = x},
    FoldColumn         = {fg = c5, bg =  x, style = x},
    Folded             = {fg = c3, bg = c0, style = x},
    Function           = {fg = cC, bg =  x, style = x},
    Identifier         = {fg =  x, bg =  x, style = x},
    Ignore             = {fg = c1, bg =  x, style = x},
    Include            = {fg = cD, bg =  x, style = 'bold'},
    IncSearch          = {fg = c0, bg = cB, style = x}, -- search results and interactive replace
    Keyword            = {fg = cE, bg =  x, style = 'bold'},
    Label              = {fg = c5, bg =  x, style = 'bold'},
    LineNr             = {fg = c3, bg =  x, style = x},
    Macro              = {fg = c5, bg =  x, style = x},
    MatchParen         = {fg =  x, bg = c2, style = 'bold'}, -- bracket under cursor
    ModeMsg            = {fg = c5, bg =  x, style = x}, -- showmode
    MoreMsg            = {fg = c5, bg =  x, style = 'bold'}, -- screen filled with messages prompt
    NonText            = {fg = c3, bg =  x, style = 'bold'},
    Normal             = {fg = c5, bg =  x, style = x},
    NormalFloat        = {fg = c5, bg =  x, style = x},
    NormalNC           = {fg = c5, bg =  x, style = x},
    Number             = {fg = cB, bg =  x, style = 'bold'},
    Operator           = {fg = c5, bg =  x, style = 'bold'}, -- =, +, and, or, etc.
    Parameter          = {fg = cE, style = x},
    Pmenu              = {fg = c5, bg = c1, style = x}, -- popup menu
    PmenuSbar          = {fg = c5, bg =  x, style = x}, -- popup menu scrollbar track
    PmenuSel           = {fg = c0, bg = c5, style = x}, -- popup menu selected item
    PmenuThumb         = {fg = c5, bg = c3, style = x}, -- popup menu scrollbar slider
    PreCondit          = {fg = c5, bg =  x, style = x},
    PreProc            = {fg = c5, bg =  x, style = x},
    Question           = {fg = c5, bg =  x, style = x},
    QuickFixLine       = {fg = c5, bg =  x, style = x},
    Repeat             = {fg = cA, bg =  x, style = 'bold'}, -- for, while, etc.
    Search             = {fg =  x, bg =  x, style = 'reverse,bold'}, -- hlsearch results and inccommand
    SignColumn         = {fg = c5, bg =  x, style = x},
    Special            = {fg = c5, bg =  x, style = x},
    SpecialChar        = {fg = c7, bg =  x, style = x}, -- escape chars
    SpecialComment     = {fg = cB, bg =  x, style = 'bold'}, -- all caps words followed by :
    SpecialKey         = {fg = c3, bg =  x, style = 'bold'},
    SpellBad           = {style = 'undercurl', sp = c8},
    SpellCap           = {style = 'undercurl', sp = cD}, -- word that should be capitalized
    SpellLocal         = {style = 'undercurl', sp = cB}, -- word from another region
    SpellRare          = {style = 'undercurl', sp = cE},
    Statement          = {fg = c5, bg =  x, style = x},
    StatusLine         = {fg = c5, bg =  x, style = x},
    StatusLineNC       = {fg = c5, bg =  x, style = x},
    StatusLineTerm     = {fg = c5, bg =  x, style = x},
    StatusLineTermNC   = {fg = c5, bg =  x, style = x},
    StorageClass       = {fg = c5, bg =  x, style = 'italic'},
    String             = {fg = cB, bg =  x, style = x},
    Structure          = {fg = c5, bg =  x, style = x},
    TabLine            = {fg = c5, bg =  x, style = x},
    TabLineFill        = {fg = c5, bg =  x, style = x},
    TabLineSel         = {fg = c5, bg =  x, style = x},
    Tag                = {fg = c5, bg =  x, style = 'bold'},
    Title              = {fg = c5, bg =  x, style = 'bold'},
    Todo               = {fg = cC, bg =  x, style = 'bold'}, -- todo, hack, warning
    Type               = {fg = c5, bg =  x, style = 'bold,italic'},
    Typedef            = {fg = c5, bg =  x, style = 'italic'},
    Underlined         = {fg = c5, bg =  x, style = 'underline'},
    VertSplit          = {fg = c2, bg =  x, style = x},
    Visual             = {fg =  x, bg =  x, style = 'reverse'}, -- selected text in visual mode
    -- Visual             = {fg =  x, bg = c2, style = x}, -- selected text in visual mode
    VisualNOS          = {fg = c5, bg =  x, style = x},
    WarningMsg         = {fg = cA, bg =  x, style = 'bold'}, -- fixme, xxx, bug
    Whitespace         = {fg = c3, bg =  x, style = 'bold'}, -- trailing space, tab, etc.
    WildMenu           = {fg = c5, bg =  x, style = x},

    -- CUSTOM:
    Header             = {fg = cD, bg =  x, style = x},
    Indent             = {fg = c2},

    -- DIAGNOSTICS:
    DiagnosticError    = {fg = c8, bg =  x, style = x},
    DiagnosticWarn     = {fg = cA, bg =  x, style = x},
    DiagnosticInfo     = {fg = cD, bg =  x, style = x},
    DiagnosticHint     = {fg = c3, bg =  x, style = x},
    DiagnosticUnderlineError = {sp = c8},
    DiagnosticUnderlineWarn  = {sp = cA},
    DiagnosticUnderlineInfo  = {sp = cD},
    DiagnosticUnderlineHint  = {sp = c3},

    -- TYPE: man
    ManFooter          = {fg = c3, bg =  x, style = 'bold'}, -- footer
    ManHeader          = {fg = c3, bg =  x, style = 'bold'}, -- header
    ManLowerSentence   = {fg =  x, bg =  x, style = 'italic'}, -- ?
    ManOptionDesc      = {fg = cE, bg =  x, style = 'bold'}, -- parameters
    ManReference       = {fg = cA, bg =  x, style = x}, -- links to other man pages
    ManSectionHeading  = {fg = cB, bg =  x, style = 'bold'}, -- heading
    ManSentence        = {fg =  x, bg =  x, style = 'italic'}, -- ?
    ManSubHeading      = {link = 'include'}, -- sub heading

    -- TYPE: help
    helpCommand        = {fg = cD, bg =  x, style = x},
    helpExample        = {fg = cA, bg =  x, style = x},
    helpHyperTextEntry = {fg = cC, bg =  x, style = 'italic,underline'},
    helpHyperTextJump  = {fg = cF, bg =  x, style = x},
    helpNote           = {fg =  x, bg =  x, style = 'bold'},
    helpOption         = {fg = cC, bg =  x, style = x},
    helpSpecial        = {fg = cE, bg =  x, style = x},

    -- TYPE: desktop
    dtBooleanKey       = {link = 'dtStringKey'},
    dtCategoriesKey    = {link = 'dtStringKey'},
    dtDelim            = {link = 'Operator'},
    dtExec             = {link = 'String'},
    dtExecKey          = {link = 'dtStringKey'},
    dtGroup            = {link = 'Header'},
    dtIconStringKey    = {link = 'dtStringKey'},
    dtLocalestring     = {link = 'String'},
    dtLocalestringKey  = {link = 'dtStringKey'},
    dtStringKey        = {fg = cE, style = x},
    dtTypeKey          = {link = 'dtStringKey'},
    dtVersionKey       = {link = 'dtStringKey'},
    dtXExtensionKey    = {link = 'dtStringKey'},

    -- TYPE: cfg
    CfgOnOff           = {link = 'Boolean'},
    CfgParams          = {fg = cA, style = x}, -- ?
    CfgSection         = {fg = cD, style = x},
    CfgValues          = {fg = cC},

    -- TYPE: dosini
    dosiniHeader       = {link = 'Header'},
    dosiniLabel        = {link = 'Label'},
    dosiniValue        = {link = 'CfgValues'},

    -- TYPE: batch
    dosbatchImplicit   = {link = 'Builtin'},

    -- TYPE: powershell
    ps1Cmdlet          = {link = 'Builtin'},
    ps1Function        = {link = 'TSFunction'},
    ps1Variable        = {link = 'TSVariable'},

    -- TYPE: xml
    xmlAttrib          = {fg = cE, bg =  x, style = x},
    xmlEqual           = {link = 'Operator'},
    -- xmlProcessing      = {fg = c3, bg =  x, style = x},
    xmlDocType         = {fg =  x, bg =  x, style = 'bold'},
    xmlDocTypeDecl     = {link = 'Bracket'},
    xmlProcessingDelim = {link = 'Bracket'},
    xmlTag             = {link = 'Bracket'},
    xmlTagName         = {link = 'Function'},

    -- TYPE: fstab
    fsMountPoint       = {fg = cC, style = 'bold'},

    -- TYPE: javascript
    javascriptTSConstructor = {link = 'Variable'},

    -- TYPE: udev
    udevrulesAssignKey = {link = 'Builtin'},
    udevrulesDelimiter = {link = 'Bracket'},
    udevrulesEnvVar    = {link = 'Constant'},
    udevrulesPath      = {link = 'Function'},
    udevrulesRuleEq    = {link = 'Operator'},
    udevrulesRuleKey   = {link = 'Builtin'},
    udevrulesRuleTest  = {link = 'Operator'},
    udevrulesVariable  = {link = 'Constant'},

    -- TYPE: passwd
    passwdPasswordColon = {link = 'Delimiter'},
    passwdUIDColon =      {link = 'Delimiter'},
    passwdGIDColon =      {link = 'Delimiter'},
    passwdGecosColon =    {link = 'Delimiter'},
    passwdDirColon =      {link = 'Delimiter'},
    passwdShellColon =    {link = 'Delimiter'},
    passwdPasswordColon = {link = 'Delimiter'},
    passwdShell =         {link = 'Function'},
    passwdDir =           {link = 'Builtin'},
    passwdAccount =       {link = 'Field'},
    passwdGecos =         {link = 'parameter'},

    -- TYPE: norg
    NeorgTodoItem1 = {fg =  x, style = x},
    NeorgUnorderedList1 = {fg = c3, style = 'bold'},
    NeorgTodoItem1Undone = {fg = c3, style = x},
    NeorgTodoItem1OnHold = {fg = cD, style = x},
    NeorgTodoItem1Done = {fg = cB, style = x},
    NeorgTodoItem1Uncertain = {fg = cE, style = x},
    NeorgTodoItem1Urgent = {fg = cA, style = x},
    NeorgTodoItem1Cancelled = {fg = cF, style = x},
    NeorgTodoItem1Pending = {fg = cC, style = x},
    NeorgTodoItem1Recurring = {fg = c9, style = x},

    -- TYPE: registry
    registryPath    = {fg = cD, style = x},
    registrySpecial = {fg = cD, style = x},
    registrySubKey  = {link = 'bold'},

    -- TYPE: i3 config
    i3ConfigAction = {fg = cE},
    i3ConfigBindKeyword = {fg = cD},
    i3ConfigBlock = {fg = cF},
    i3ConfigBlockKeyword = {fg = cD},
    i3ConfigFontKeyword = {fg = cD},
    i3ConfigInitialize = {fg = cC},
    i3ConfigInitializeKeyword = {fg = cD},
    i3ConfigNumber = {link = 'number'},
    i3ConfigUnitOr = {link = 'operator'},
    i3ConfigUnit = {link = 'number'},
    i3ConfigVariableAndModifier = {link = 'number'},

    -- TYPE: python
    pythonBuiltin = {link = 'builtin'},
    pythonEscape = {link = 'specialchar'},
    pythonStatement = {link = 'parameter'},

    -- STATUSLINE:
    StatusD            = {fg = c5, bg = c1, style = 'bold'}, -- default
    StatusC            = {fg = c0, bg = c5, style = 'bold'}, -- command
    StatusC2           = {fg = c5, bg = c0, style = 'bold'}, -- command deco
    StatusI            = {fg = c0, bg = cB, style = 'bold'}, -- insert
    StatusI2           = {fg = cB, bg = c0, style = 'bold'}, -- insert deco
    StatusMod          = {fg = cD, bg =  x, style = 'bold'}, -- modified
    StatusN            = {fg = c5, bg = c1, style = 'bold'}, -- normal
    StatusN2           = {fg = c1, bg = c0, style = 'bold'}, -- normal deco
    StatusR            = {fg = c0, bg = cA, style = 'bold'}, -- replace
    StatusR2           = {fg = cA, bg = c0, style = 'bold'}, -- replace deco
    StatusRe           = {fg = c0, bg = c9, style = 'bold'}, -- interactive replace
    StatusRe2          = {fg = c9, bg = c0, style = 'bold'}, -- interactive replace deco
    StatusT            = {fg = c0, bg = cE, style = 'bold'}, -- terminal
    StatusT2           = {fg = cE, bg = c0, style = 'bold'}, -- terminal deco
    StatusV            = {fg = c0, bg = cD, style = 'bold'}, -- visual
    StatusV2           = {fg = cD, bg = c0, style = 'bold'}, -- visual deco
    StatusS            = {fg = c0, bg = c8, style = 'bold'}, -- select
    StatusS2           = {fg = c8, bg = c0, style = 'bold'}, -- select deco
    Status1            = {fg = c3, bg =  x, style = 'bold'},
    Status2            = {fg = c3, bg =  x, style = 'strikethrough'},
    Status3            = {fg = cE, bg =  x, style = 'bold,italic'},
    StatusUnknown      = {fg = c0, bg = cF, style = 'bold'},
    StatusUnknown2     = {fg = cF, bg = c0, style = 'bold'},

    -- STARTIFY:
    StartifyBracket    = {link = 'Bracket'},
    StartifyFile       = {fg = c5, style = 'bold'},
    StartifyPath       = {fg = c3, style = 'bold'},
    StartifySlash      = {fg = c3, style = 'bold'},
    StartifySpecial    = {fg = c3, style = 'bold'},
    -- StartifyNumber     = {fg = cC, style = 'bold'},

    -- INDENTLINE:
    IndentBlanklineChar = {link = 'Indent'},

    -- STARTUPTIME:
    startuptimeHeader        = {fg = c3, style = 'bold'},
    startuptimeOtherEvent    = {fg = c5, style = x},
    startuptimePercent       = {fg = cE, style = x},
    startuptimeSourcingEvent = {fg = cD, style = x},
    startuptimeStartupKey    = {fg = c5, style = x},
    startuptimeStartupValue  = {fg = c5, style = 'bold'},
    startuptimeTime          = {fg = cC, style = x},

    -- TREESITTER:
    -- TSConstBuiltin     = {link = 'Boolean'},
    TSConstBuiltin     = {fg = c9, bg =  x, style = 'italic'},
    TSKeywordReturn    = {fg = cA, bg =  x, style = x},
    TSConstMacro       = {link = 'Include'},
    TSConstructor      = {link = 'Bracket'},
    TSError            = {fg =  x, style = 'underline', sp = c8},
    TSFuncBuiltin      = {link = 'Builtin'},
    TSKeywordFunction  = {fg = cE, style = 'italic'},
    TSParameter        = {link = 'parameter'},
    TSProperty         = {fg = cC, style = x},
    TSPunctBracket     = {link = 'Bracket'},
    TSPunctSpecial     = {fg = cF, style = x},
    TSStringRegex      = {link = 'String'},
    TSVariable         = {fg = c5},
    -- TSConditional      = {},
    -- TSConstant         = {},
    -- TSConstructor      = {fg = cC, style = 'nocombine'},
    -- TSEmphasis         = {},
    -- TSException        = {},
    TSField            = {link = 'Field'},
    -- TSFloat            = {},
    -- TSFuncMacro        = {},
    -- TSFunction         = {},
    -- TSInclude          = {},
    -- TSKeyword          = {},
    TSKeywordOperator  = {link = 'keyword'},
    -- TSLabel            = {},
    -- TSLiteral          = {},
    -- TSMethod           = {},
    -- TSNamespace        = {},
    -- TSNone             = {},
    -- TSNumber           = {},
    -- TSOperator         = {},
    -- TSParameterReferenc= {},
    -- TSPunctDelimiter   = {},
    -- TSRepeat           = {},
    -- TSStrike           = {},
    -- TSString           = {},
    -- TSStringEscape     = {},
    -- TSStringRegex      = {fg = cC, style = 'nocombine'},
    -- TSSymbol           = {},
    -- TSTag              = {},
    -- TSTagDelimiter     = {},
    -- TSText             = {},
    -- TSTitle            = {},
    -- TSType             = {},
    -- TSTypeBuiltin      = {},
    -- TSUnderline        = {},
    -- TSURI              = {},
    -- TSVariableBuiltin  = {},
}

for i in pairs(theme) do
    if theme[i].link then -- check if link exists
        -- cmd('hi! link '..i..' '..theme[i].link) -- apply link
        vim.api.nvim_set_hl(0, i, {link = theme[i].link})
    else -- check if each property exists
        local fg    = theme[i].fg    and ' guifg='..theme[i].fg    or ''
        local bg    = theme[i].bg    and ' guibg='..theme[i].bg    or ''
        local style = theme[i].style and ' gui='  ..theme[i].style or ''
        local sp    = theme[i].sp    and ' guisp='..theme[i].sp    or ''
        cmd('hi '..i..fg..bg..style..sp) -- apply properties
    end
end
