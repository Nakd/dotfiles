-- if pcall(check(plugin)) then
--     -- do stuff
-- end

local install_path = fn.stdpath('data')..'/site/pack/paqs/start/paq-nvim'
if fn.empty(fn.glob(install_path)) > 0 then
    print('Installing Paq...')
    paq_bootstrap = fn.system {
        'git',
        'clone',
        '--depth',
        '1',
        'https://github.com/savq/paq-nvim',
        install_path,
    }
    print('Paq installed. Restart Neovim.')
end
--
require 'paq' {
    'savq/paq-nvim';
    {'nvim-treesitter/nvim-treesitter',
        run = require 'plugins/treesitter'};
    'nvim-treesitter/playground';
    {'lukas-reineke/indent-blankline.nvim',
        run = cmd('source ~/.config/nvim/vim/indentline.vim')};
    -- {'windwp/nvim-autopairs',
    --     run = require 'plugins/autopairs'};
    'tpope/vim-surround';
    {'mhinz/vim-startify',
        run = cmd('source ~/.config/nvim/vim/startify.vim')};
    -- {'goolord/alpha-nvim',
    --     run = require'alpha'.setup()};
    'norcalli/nvim-colorizer.lua';
    'dstein64/vim-startuptime';
    -- 'bling/vim-bufferline'; -- replace with buffers in statusline
    -- 'tpope/vim-commentary';
    {'numToStr/Comment.nvim',
        run = require('Comment').setup()}
    -- 'folke/which-key.nvim'; -- show possible commands when typing
}

-- function check(plugin)
--     if pcall(require, plugin) then
--         return true
--     else
--         return false
--     end
-- end

require 'plugins/treesitter'
require 'plugins/autopairs'
require 'plugins/colorizer'
require 'plugins/bufferline'

function PConfig(plugin)
    return function()
        if pcall(require, plugin) then
            require("main.plugin_config."..plugin.."_config")()
        end
    end
end

require("paq") {
    {"folke/which-key.nvim", run = PConfig("which-key")}
}
