-- local home = vim.fn.expand("$HOME")
local o = vim.opt

-- O.fixendofline = false
-- O.indentkeys   = ""
-- O.selectmode   = "mouse"
-- O.splitbelow   = true
o.autochdir       = true
-- o.backup          = false
o.breakindent     = true -- preserve indents when wrapping
o.commentstring   = "#%s"
o.cursorline      = true
o.fillchars       = "eob: ,stl: ,fold: " -- remove tilde from blank lines ─
-- o.fillchars:append("stl:-")
o.guicursor       = "n-v-sm:block,i-ci-c-ve:ver25,r-cr-o:hor20"
o.hidden          = true -- allow swapping from modififed buffer
o.history         = 200 -- length of various histories
o.hlsearch        = false -- don"t highlight every match
o.ignorecase      = true -- case-insensitive searching
o.laststatus      = 3 -- statusline: 2 = normal, 3 = global
o.lazyredraw      = true
o.linebreak       = true -- don"t break words
o.list            = true -- display whitespace chars
o.listchars       = "trail:•,tab:   ,extends:›,precedes:‹" -- whitespace chars; precedes:◀,extends:▶├
o.mouse           = "nvi" -- enable mouse use
o.numberwidth     = 1
o.scrolloff       = 99 -- vertical cursor padding
o.secure          = true
o.shell           = "/bin/sh"
o.showbreak       = "" -- ↳
o.showmode        = false -- don"t print mode in bottom line
o.sidescrolloff   = 8 -- horizontal cursor padding
o.signcolumn      = "no"
o.smartcase       = true -- case-sensitive if using uppercase letters
o.splitright      = true
o.swapfile        = false
o.termguicolors   = true -- enables 24-bit color
o.title           = true -- sets window title
o.undodir         = "/tmp/nvim/undo" -- "persistent" undo
o.undofile        = true -- persistent undo
o.virtualedit     = "block" -- allow rectangle visual selection regardless of current line length
o.wrap            = false
-- o.writebackup     = false

o.formatoptions:remove("o") -- don't start comment on newline if current line is a comment
o.nrformats:append("alpha") -- increment letters with C-a
o.shortmess:append("Ss") -- remove info from cmdline

-- TABS_SPACES:
local tab = 4
o.expandtab   = false -- true = convert tabs to spaces
o.shiftwidth  = tab
o.softtabstop = tab
o.tabstop     = tab

-- FOLDS:
-- TODO filetype specific
-- O.foldexpr  = "nvim_treesitter#foldexpr()"
-- O.foldminlines = 2
-- O.foldtext  = "getline(v:foldstart).'...'.trim(getline(v:foldend))..' ('..(v:foldend - v:foldstart + 1)..')'"
o.foldmethod   = "marker"
o.foldtext     = "substitute(getline(v:foldstart),'\t',repeat(' ',&tabstop),'g').'...'.trim(getline(v:foldend))..' ('..(v:foldend - v:foldstart + 1)..')'"

if vim.g.neovide then
	o.guifont = "JetBrains Mono:h14"
end
