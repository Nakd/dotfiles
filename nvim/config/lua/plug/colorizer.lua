require("colorizer").setup(
	{"*", "!packer"},
	{
		RRGGBBAA = true,
		rgb_fn = true,
		hsl_fn = true,
		names = false,
		-- mode = "foreground",
	}
)
