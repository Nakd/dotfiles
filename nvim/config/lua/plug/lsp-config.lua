require("plug/lspinstaller")

local diagnostic = vim.diagnostic
local lsp = vim.lsp

diagnostic.config({
	severity_sort = true,
	signs = false,
	update_in_insert = false,
	virtual_text = {
		spacing = 0,
		prefix = "◆", --     
		-- BUG https://github.com/neovim/neovim/blob/ed6bbc03af7be192e3d615f8ee761611e78d9881/runtime/lua/diagnostic.lua#L1062
		-- '%s %s' -> '%s%s'

		-- TODO figure out how to change severity of certain messages (not just inline diag)
		-- format = function(diag)
		-- 	-- 
		-- 	if diag.severity == diagnostic.severity.ERROR then
		-- 		return diag.message .. " "
		-- 	elseif diag.severity == diagnostic.severity.WARN then
		-- 		if diag.message:match("line too long") then
		-- 			diag.severity = diagnostic.severity.INFO
		-- 		end
		-- 		return diag.message .. " "
		-- 	elseif diag.severity == diagnostic.severity.INFO then
		-- 		return diag.message .. " "
		-- 	elseif diag.severity == diagnostic.severity.HINT then
		-- 		return diag.message .. " "
		-- 	end
		-- end
	},
})

lsp.handlers["textDocument/hover"] = lsp.with(lsp.handlers.hover, {
	border = "rounded",
})

local hide = false
local function maps()
	Mapb("n", "gD", function()
		if hide then
			vim.diagnostic.show()
			hide = false
		else
			vim.diagnostic.hide()
			hide = true
		end
	end, "get info about current symbol")
	Mapb("n", "K", lsp.buf.hover, "get info about current symbol")
	Mapb("n", "gj", function()
		if #diagnostic.get(0) > 0 then
			diagnostic.goto_next({float = {source = true, header = "", border = "rounded"}})
			-- diagnostic.goto_next({float = false})
		end
	end, "go to next diagnostic")
	Mapb("n", "gk", function()
		if #diagnostic.get(0) > 0 then
			diagnostic.goto_prev({float = {source = true, header = "", border = "rounded"}})
			-- diagnostic.goto_next({float = false})
		end
	end, "go to prev diagnostic")
	Mapb("n", "gd", lsp.buf.definition, "go to definition")
	Mapb("n", "gR", lsp.buf.rename, "rename symbol")
	Mapb("n", "gl", function()
		diagnostic.open_float({source = true, header = "", border = "rounded"})
	end, "open diagnostic window for current line")
end

require("lspconfig").sumneko_lua.setup({
	on_attach = maps,
	settings = {
		Lua = {
			runtime = {version = "LuaJIT"},
			telemetry = {enable = false},
			type = { -- toggle certain errors / warnings
				castNumberToInteger = true,
			},
			diagnostics = {
				globals = {"vim"},
			},
		},
	},
})
require("lspconfig").pylsp.setup({
	on_attach = maps,
	settings = {
		pylsp = {
			plugins = {
				pycodestyle = {
					maxLineLength = 95,
				},
			},
		},
	},
})
require("lspconfig").gopls.setup({
	on_attach = maps,
})
require("lspconfig").html.setup({
	on_attach = maps,
})
-- require("lspconfig").bashls.setup({ -- needs npm
-- 	on_attach = maps,
-- })
