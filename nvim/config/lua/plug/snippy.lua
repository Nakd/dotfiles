local snippy = require("snippy")
snippy.setup({
	snippet_dirs = "~/.config/nvim/snips",
	enable_auto = true,
	-- scopes = {lua = {"lua"}}
})

Map("i", "<c-j>", snippy.expand_or_advance, "snippy expand/next")
Map("i", "<c-k>", snippy.previous, "snippy prev")
-- function()
-- 	print(snippy.expand_or_advance)
-- end)
