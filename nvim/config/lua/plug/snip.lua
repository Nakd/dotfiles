-- require("luasnip.loaders.from_snipmate").lazy_load({paths = "~/.config/nvim/snippets"})
local ls = require("luasnip")
local s = ls.snippet
local i = ls.insert_node
local fmt = require("luasnip.extras.fmt").fmt
local t = ls.text_node
-- local sn = ls.snippet_node
-- local isn = ls.indent_snippet_node
local f = ls.function_node
local c = ls.choice_node
-- local d = ls.dynamic_node
local r = require("luasnip.extras").rep
-- local r = ls.restore_node

ls.config.set_config({
	history = true,
	update_events = "TextChanged,TextChangedI",
	enable_autosnippets = true,
})

Map({"i", "s"}, "<c-j>", function()
	-- BUG seems buggy?
	if ls.expand_or_jumpable() then
		ls.expand_or_jump()
	end
end)
Map({"i", "s"}, "<c-k>", function()
	-- BUG seems buggy?
	if ls.jumpable(-1) then
		ls.jump(-1)
	end
end)
-- Map({"i", "s"}, "<c-l>", function() require("luasnip.extras.select_choice")() end)

local function add(T) -- makes syntax better
	assert(type(T) == "table", "add() expects a table")
	-- local filetype, trigger, text, nodes, mode
	for ft in pairs(T) do
		local filetype = ft
		-- TODO collect snips, then ls.add_snippets all at once
		-- local snips = {
		-- 	auto = {},
		-- 	manual = {},
		-- }
		for trig, opts in pairs(T[ft]) do
			local trigger = trig
			local text = opts.text or opts[1] or trig
			local nodes = opts.nodes or {}
			local mode = opts.auto and "autosnippets" or "snippets"
			local regex = opts.regex or false
			-- if regex then
			-- 	print(s.captures)
			-- end
			-- print(trigger, text, nodes, mode)
			ls.add_snippets(filetype, {s({trig = trigger, regTrig = regex}, fmt(text, nodes))}, {type = mode})
		end
	end
end

-- SNIPS:
ls.cleanup() -- remove all snippets (to cleanly source this file)
local function space() -- makes replacing captured space (%s) easier
	return function(_, snip) return snip.captures[1] end
end
local snips = {}
snips.lua = {
	["test"] = {""},
	["f"] = {
		text = [[
		local function {}({})
		end
		]],
		nodes = {i(1, "name"), i(2)}
	},
	["F"] = {
		text = [[
		function {}({})
			{}
		end
		]],
		nodes = {i(1, "Name"), i(2), i(3)}
	},
	["if"] = {
		text = [[
		if {} then
			{}
		end
		]],
		nodes = {i(1, "true"), i(2)},
		-- auto = true,
	},
	["fp"] = {
		text = [[
		for {}, {} in pairs({}) do
			{}
		end
		]],
		nodes = {i(1, "k"), i(2, "v"), i(3, "table"), i(4)},
		-- auto = true,
	},
	["fi"] = {
		text = [[
		for {}, {} in ipairs({}) do
			{}
		end
		]],
		nodes = {i(1, "i"), i(2, "v"), i(3, "table"), i(4)},
		-- auto = true,
	},
}
snips.python = {
	["ifn"] = {
		text = [[
		if __name__ == "__main__":
			main()
		]],
		-- nodes = {i(1, "file"), i(2, "mode"), i(3, "fd"), i(0, "pass")},
		-- auto = true,
	},
	-- ["f"] = {
	-- 	text = [[
	-- 	def {}({}):
	-- 	]],
	-- 	nodes = {i(1, "name"), i(2), i(3)}
	-- },
	["wo "] = {
		text = [[
		with open({}, "{}") as {}:
			{}
		]],
		nodes = {i(1, "file"), i(2, "mode"), i(3, "fd"), i(0, "pass")},
		auto = true,
	}
}
snips.go = {
	-- TODO figure out how to do regex snips (condition instead?)
	["print"] = {
		text = [[
		fmt.Println({})
		]],
		nodes = {i(1)},
		auto = true
	},
	["sleep"] = {
		text = [[
		time.Sleep({} * time.{})
		]],
		-- TODO figure out choice nodes
		nodes = {i(1), c(2, {t("Second"), t("Millisecond")})},
		-- auto = true
	},
}
snips.css = {
	["!;"] = {
		text = [[
		!important;
		]],
		auto = true
	}
}
snips.html = {
	["^(%s*)<"] = {
		text = [[
		{}<{}>
			{}{}
		{}</{}>
		]],
		nodes = {f(space()), i(1), f(space()), i(2), f(space()), r(1)},
		-- auto = true,
		regex = true,
	},
	["^(%s*)a "] = {
		text = [[
		{}<a href="{}">
			{}{}
		{}</a>
		]],
		nodes = {f(space()), i(1), f(space()), i(2), f(space())},
		auto = true,
		regex = true,
	},
	-- ["test"] = {
	-- 	text = [[
	-- 	TEST
	-- 		abc
	-- 	123
	-- 	]],
	-- 	-- nodes = {f(space())},
	-- 	auto = true,
	-- 	-- regex = true,
	-- }
}
add(snips) -- add all snippets
