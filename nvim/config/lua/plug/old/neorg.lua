require('neorg').setup {
    load = { -- Tell Neorg what modules to load
        ["core.defaults"] = {}, -- Load all the default modules
        ["core.keybinds"] = { -- Configure core.keybinds
            config = {
                default_keybinds = true, -- Generate the default keybinds
                neorg_leader = "<Leader>o", -- This is the default if unspecified
                hook = function(keybinds)
                    keybinds.unmap("norg", "n", "<C-s>")
                end,
            }
        },
        ["core.norg.concealer"] = {
            config = {
                icons = {
                    done = {
                        enabled = true,
                        icon = "-",
                    },
                    uncertain = {
                        enabled = true,
                        icon = "?",
                        highlight = "NeorgTodoItemUncertainMark",
                        query = "(todo_item_uncertain) @icon",
                        extract = function()
                            return 1
                        end,
                    },
                    undone = {
                        enabled = true,
                        icon = " ",
                        highlight = "NeorgTodoItemUndoneMark",
                        query = "(todo_item_undone) @icon",
                        extract = function()
                            return 1
                        end,
                    },

                },
            },
        }, -- Allows for use of icons
    }
}
