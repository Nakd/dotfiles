local null_ls = require("null-ls")

-- register any number of sources simultaneously
-- local sources = {
--     null_ls.builtins.formatting.prettier,
--     null_ls.builtins.diagnostics.write_good,
--     null_ls.builtins.code_actions.gitsigns,
-- }

-- null_ls.config({ sources = sources })

null_ls.config({
    sources = {
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.diagnostics.luacheck,
    }
})

require("lspconfig")["null-ls"].setup({
    -- see the nvim-lspconfig documentation for available configuration options
    on_attach = my_custom_on_attach
})
