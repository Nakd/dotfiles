-- local actions = require("telescope.actions")
require("telescope").setup({
	defaults = {
		selection_caret = "▶ ",
		mappings = {
			i = {
				["<esc>"] = "close",
				["<c-j>"] = "move_selection_next",
				["<c-k>"] = "move_selection_previous",
				["<c-u>"] = false, -- clear line
			},
		},
		layout_config = {
			preview_cutoff = 90, -- TODO preview doesn't come back after resize
			horizontal = {
				height = { padding = 1 },
				width = { padding = 2 },
			},
		},
	},
	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true,
			override_file_sorter = true,
			case_mode = "smart_case",
		},
	},
})

require("telescope").load_extension("fzf")
