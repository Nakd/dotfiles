-- TODO popup to select which files to search through,
-- (current dir, configs, etc.)
require("fzf-lua").setup {
    win_opts = {
        number = false,
        hl = {
            normal = "Normal"
        },
    },
    fzf_args = vim.env.FZF_DEFAULT_OPTS
    -- fzf_opts = {
    --     ["--layout"] = "default",
    --     -- ["--info"] = "inline",
    -- },
    -- fzf_colors = {
    --     ["fg"]      = { "fg", "Normal" },
    --     ["bg"]      = { "bg", "Normal" },
    --     ["hl"]      = { "fg", "Include" },
    --     ["fg+"]     = { "fg", "Normal" },
    --     ["bg+"]     = { "bg", "CursorLine" },
    --     ["hl+"]     = { "fg", "Statement" },
    --     ["info"]    = { "fg", "Comment" },
    --     ["prompt"]  = { "fg", "String" },
    --     ["pointer"] = { "fg", "Normal" },
    --     ["marker"]  = { "fg", "Normal" },
    --     ["spinner"] = { "fg", "Label" },
    --     ["header"]  = { "fg", "Comment" },
    --     ["gutter"]  = { "bg", "Normal" },
    -- },
}
