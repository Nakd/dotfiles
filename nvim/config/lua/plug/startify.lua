local home = vim.fn.expand("$HOME")
vim.g.startify_custom_header = ""
vim.g.startify_session_autoload = 1
vim.g.startify_bookmarks = {{i = home .. "/dots/nvim/config/init.lua"}}
vim.g.startify_files_number = 30
vim.g.startify_change_to_dir = 0
vim.g.startify_lists = {
	{type = "files"},
	{type = "sessions", header = {"   Sessions"}},
	{type = "bookmarks", header = {"   Bookmarks"}},
	-- {type = 'commands', header = {'   Commands'}       },
}
