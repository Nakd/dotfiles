vim.wo.colorcolumn = "9999" -- fixes cursorline hl getting stuck
vim.g.indent_blankline_filetype_exclude = {
	"diff",
	"help",
	"text",
	"man",
	"startify",
	"conf",
	"packer",
	"norg",
	"startuptime",
	"",
	"lsp-installer",
	"txt",
}
