local g = vim.g
g.python3_host_prog = "python3" -- improves startup time
g.python_host_prog = "python2" -- improves startup time
g.qf_disable_statusline = true -- disable quickfix statusline
g.load_black = false
