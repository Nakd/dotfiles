-- silent maps
function Map(mode, key, action, desc)
	vim.keymap.set(mode, key, action, { desc = desc, silent = true })
end

-- loud maps
function Mapl(mode, key, action, desc)
	vim.keymap.set(mode, key, action, { desc = desc, silent = false })
end

-- buffer maps
function Mapb(mode, key, action, desc)
	vim.keymap.set(mode, key, action, { desc = desc, silent = true, buffer = 0 })
end

-- loud buffer maps
function Mapbl(mode, key, action, desc)
	vim.keymap.set(mode, key, action, { desc = desc, silent = false, buffer = 0 })
end

-- make space do nothing on its own
Map("", "<space>", "<nop>")

vim.g.mapleader = " "

-- save file
Map({"n", "x"}, "<c-s>", "<cmd>update<cr>")
Map({"n", "x"}, "<c-a-s>", "<cmd>wa<cr>")
Map("i", "<c-s>", "<esc><cmd>update<cr>")
Map("i", "<c-a-s>", "<esc><cmd>wa<cr>")

-- quit
Map("n", "<leader>q", "<cmd>q<cr>")

-- replace
Mapl("n", "<leader>r", ":%s//g<left><left>")
Mapl("x", "<leader>r", ":s//g<left><left>")

-- replace with confirmation
Mapl("n", "<leader>R", ":%s//gc<left><left><left>")
Mapl("x", "<leader>R", ":s//gc<left><left><left>")

-- execute current file
Map("n", "<leader>e", "<cmd>!./%<cr>")

-- copy / paste using system clipboard TODO utilize other registers
Map("n", "d", '"+d')
Map("n", "D", '"+D')
Map("n", "dd", '"+dd')
Map("n", "p", '"+p')
Map("n", "P", '"+P')
Map("n", "x", '"+x')
Map("n", "Y", '"+y$')
Map("n", "y", '"+y')
Map("x", "d", '"+d')
Map("x", "x", '"+x')
Map("x", "y", '"+ygv')
-- map('v', '<leader>y', '"+ygv')

-- toggle true / false -- TODO do this with lsp?
-- map('n', '<leader>t', 'ciwtrue<esc>')
-- map('n', '<leader>f', 'ciwfalse<esc>')

-- print current directory
Map("", "<leader>d", "<cmd>pwd<cr>")

-- buffer navigation
Map("n", "<a-h>", "<cmd>bp<cr>")
Map("n", "<a-l>", "<cmd>bn<cr>")
Map({"i", "x"}, "<a-h>", "<cmd>bp<cr><esc>")
Map({"i", "x"}, "<a-l>", "<cmd>bn<cr><esc>")

-- fold navigation
Map("n", "<a-j>", "zMzjkjzv")
Map("n", "<a-k>", "zMzkjkzv")

-- window split navigation
Map({"n", "x"}, "<c-h>", "<C-w>h")
Map({"n", "x"}, "<c-j>", "<C-w>j")
Map({"n", "x"}, "<c-k>", "<C-w>k")
Map({"n", "x"}, "<c-l>", "<C-w>l")

-- close buffer
Map("n", "<a-q>", "<cmd>bd<cr>")

-- ansi codes
Map("i", "\\e", "\\033[")
Map("i", "\\x", "\\x1b[")

-- move selection
-- map('v', '<c-j>', "<cmd>m '>+1<cr>")
-- map('v', '<c-k>', "<cmd>m '<-2<cr>")

-- show messages
Map("n", "<leader>m", "<cmd>messages<cr>")

-- vertical help
-- mapl('n', '<leader>h', ':vert bo :h ')
-- mapl("n", "<leader>h", "<cmd>Telescope help_tags<cr>")

-- remove trailing spaces
-- map('n', '<C-Space>', ':%s/ \\{1,}$//ge<cr>')

-- unmap
Map("n", "<C-z>", "")
Map("n", "Q", "")

-- move in insert mode
Map("i", "<a-j>", "<down>")
Map("i", "<a-k>", "<up>")
Map("i", "<a-h>", "<left>")
Map("i", "<a-l>", "<right>")
-- map('c', '<c-j>', '<down>')
-- map('c', '<c-k>', '<up>')
-- map('c', '<c-h>', '<left>')
-- map('c', '<c-l>', '<right>')

Map("x", "<leader>s", ":sort i<cr>")

Map("n", "<leader>t", "<cmd>so ~/.config/nvim/lua/theme.lua<cr><cmd>filetype detect<cr>")
-- Map("n", "<leader>t", "<cmd>so ~/.config/nvim/lua/theme.lua<cr><cmd>TSToggle highlight<cr><cmd>TSToggle highlight<cr>")

-- undo all since save
Map("n", "U", "<cmd>e!<cr>")

-- Map("n", "j", function()
-- 	if vim.v.count > 0 then
-- 		vim.api.nvim_feedkeys(vim.v.count .. "j", "n", true)
-- 	else
-- 		vim.api.nvim_feedkeys("j", "n", true)
-- 	end
-- end)

-- TODO floating search count + hlsearch -> hide / disable on CursorMoved (oneshot)
-- Map("n", "n", function()
-- 	vim.api.nvim_feedkeys("/\r", "n", false)
-- 	local s = vim.fn.searchcount({maxcount = 0})
-- 	if s.total > 0 then
-- 		-- nvim_create_buf, nvim_open_win, nvim_buf_set_text
-- 		vim.api.nvim_open_win(0, false, {focusable = false, relative = "cursor", row = 1, col = 0, width = 2, height = 2})
-- 	end
-- end)

-- PLUGINS:
-- update plugins
Map("n", "<leader>ps", "<cmd>so ~/.config/nvim/lua/plugins.lua<cr><cmd>PackerSync<cr>")

-- fzf
Map("n", "<leader>h", "<cmd>Telescope help_tags<cr>")
Map("n", "<leader>H", "<cmd>Telescope highlights<cr>")
-- Map("n", "<leader>f", "<cmd>Telescope find_files<cr>")
Map("n", "<leader>b", "<cmd>Telescope buffers<cr>")
-- Map("n", "<leader>o", "<cmd>Telescope oldfiles<cr>")
Map("n", "<leader>g", "<cmd>Telescope live_grep<cr>")
Map("n", "<leader>T", function()
	require("telescope.builtin").grep_string({search = "TODO", cwd = vim.fn.expand("~/dots")})
end, "find TODO files in dots")
Map("n", "<leader>D", function()
	require("telescope.builtin").diagnostics({bufnr=0})
end, "list diagnostics in current buffer")

-- find files
Map("n", "<leader>f", function()
	require("telescope.builtin").find_files({follow = true})
end, "find files in current dir")
Map("n", "<leader>F", function()
	require("telescope.builtin").find_files({cwd = "/"})
end, "find all files")
Map("n", "<leader>v", function()
	require("telescope.builtin").find_files({cwd = vim.fn.expand("~/.config/nvim"), follow = true})
end, "find nvim files")
Map("n", "<leader>o", function()
	require("telescope.builtin").oldfiles()
end, "find recently opened files")

-- Map("n", "<leader>h", "<cmd>FzfLua help_tags<cr>")
-- Map("n", "<leader>f", "<cmd>FzfLua files<cr>")
-- Map("n", "<leader>b", "<cmd>FzfLua buffers<cr>")
-- Map("n", "<leader>o", "<cmd>FzfLua oldfiles<cr>")
-- Map("n", "<leader>g", "<cmd>FzfLua live_grep_native<cr>")
-- map("n", "<leader>'", "<cmd>FZF ~<cr>")
-- map("n", "<leader>;", "<cmd>FZF<cr>")
-- map('n', '<leader>/', '<cmd>FZF /<cr>')

-- show highlight group with treesitter playground
Map("n", "<leader>c", "<cmd>TSCaptureUnderCursor<cr>")

-- align text
-- map("n", "ga", function() require("align").align_to_char(1, true) end)
-- map("x", "ga", function() require("align").align_to_char(1, true) end)
Map("n", "ga", "<Plug>(EasyAlign)ip")
Map("x", "ga", "<Plug>(EasyAlign)")
Map("n", "gA", "<cmd>ascii<cr>")

-- TOGGLES:
Map("n", "<leader><leader>w", "<cmd>set wrap!<cr>")
Map("n", "<leader><leader>s", "<cmd>set spell!<cr>")
Map("n", "<leader><leader>l", "<cmd>set list!<cr>")
Map("n", "<leader><leader>i", "<cmd>IndentBlanklineToggle<cr>")

local diag_visible = true
local function toggle_diag()
	if diag_visible then
		diag_visible = false
		vim.diagnostic.disable()
	else
		diag_visible = true
		vim.diagnostic.enable()
	end
end
Map("n", "<leader><leader>d", toggle_diag)

-- SOURCE:
Map("n", "<leader>ss", "<cmd>so ~/.config/nvim/lua/plug/snip.lua<cr>")
-- Map("n", "<leader><leader>s", "<cmd>SnippyReload<cr><cmd>so ~/.config/nvim/lua/config/snippy.lua<cr>")
