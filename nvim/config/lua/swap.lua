local words = {
	["true"] = "false",
	["True"] = "False",
	["on"] = "off",
	["yes"] = "no",
}

for k, v in pairs(words) do
	if not words[v] then
		words[v] = k
	end
end

local function swap()
	local word = vim.fn.expand("<cword>")
	if words[word] then
		vim.cmd("norm! ciw" .. words[word] .. "b")
	else
		print("[" .. word .. "] not in swap list")
	end
end

Map("n", "<leader>i", function() swap() end, "swap (invert) word under cursor")
