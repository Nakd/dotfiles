-- start flavours
local c0 = "#1B1B1B"
local c1 = "#2E2E2E"
local c2 = "#444444"
local c3 = "#7F7F7F"
local c4 = "#A4A4A4"
local c5 = "#CCCCCC"
local c6 = "#F486D0"
local c7 = "#FFFFFF"
local c8 = "#F36464"
local c9 = "#F59362"
local cA = "#F2BA61"
local cB = "#8CCA5B"
local cC = "#56CCA5"
local cD = "#67C3EE"
local cE = "#CC94F7"
local cF = "#CA9170"
-- end flavours

-- fg: foreground color
-- bg: background color
-- sp: color of under(dash/dot/curl/line)
-- link: links to the specified highlight
local x = "NONE"
local df = 100 -- diag fg opacity
local db = 15 -- diag bg opacity
local theme = {
	Bold               = {bold = 1},
	BoldItalic         = {bold = 1, italic = 1},
	Boolean            = {fg = cB, bg =  x, italic = 1},
	Bracket            = {fg = cF},
	Builtin            = {fg = cD, bg =  x},
	Character          = {fg = cB, bg =  x, underline = 1}, -- ?
	ColorColumn        = {fg = c5, bg =  x},
	Comment            = {fg = c3},
	Conceal            = {fg = c5, bg =  x},
	Conditional        = {fg = cA, bg =  x}, -- if, then, else, etc.
	Constant           = {fg = c9, bg =  x, bold = 0}, -- $ in fish and all caps var in sh
	Cursor             = {fg = c5, bg =  x},
	CursorColumn       = {link = "cursorline"},
	CursorIM           = {fg = c5, bg =  x},
	CursorLine         = {bg = c1},
	CursorLineNr       = {fg = c5, bg = c1},
	DarkenedPanel      = {fg = c5, bg =  x},
	DarkenedStatusline = {fg = c9, bg = c2},
	Debug              = {fg = c5, bg =  x, underline = 1, sp = c9}, -- ?
	Define             = {fg = c5, bg =  x},
	Delimiter          = {fg = cF, bg =  x, bold = 1},
	DiffAdd            = {fg = cB, bg =  x}, -- added line in diff mode
	DiffAdded          = {fg = cB, bg =  x},
	DiffRemoved        = {fg = c8, bg =  x},
	DiffLine           = {fg = cC, bg =  x}, -- added line in diff mode
	DiffNewFile        = {fg = cB, underline = 1, italic = 1},
	DiffOldFile        = {fg = c8, underline = 1, italic = 1},
	DiffChange         = {bg =  x, underline = 0}, -- changed line in diff mode
	DiffDelete         = {fg = c8, bg =  x}, -- deleted text in diff mode
	DiffText           = {fg = cD, bg =  x, bold = 1, underline = 1}, -- changed text in diff mode
	Directory          = {fg = c5, bg =  x},
	EndOfBuffer        = {fg = c5, bg =  x},
	Error              = {link = "TSError"},
	ErrorMsg           = {fg = c8, bg =  x}, -- error in command line
	Exception          = {fg = cA, bg =  x, italic = 1}, -- try, catch, etc.
	Field              = {fg = c5, bold = 1},
	Float              = {link = "Number"},
	FloatBorder        = {fg = c2, bg =  x},
	FoldColumn         = {fg = c5, bg =  x},
	Folded             = {fg = c3, bg = c0, underline = 0, bold = 1},
	Function           = {fg = cC, bg =  x, bold = 0},
	Identifier         = {bg =  x},
	Ignore             = {fg = c1, bg =  x},
	Include            = {fg = cD, bg =  x, bold = 1, italic = 0},
	IncSearch          = {fg = c0, bg = cB}, -- search results and interactive replace
	Keyword            = {fg = cE, bg =  x, bold = 1, italic = 0},
	Label              = {fg = c5, bg =  x, bold = 1},
	LineNr             = {fg = c3, bg =  x},
	Macro              = {fg = c7, bg =  x, underline = 0, sp = cD},
	MatchParen         = {fg =  x, bg =  x, bold = 1, underline = 1}, -- bracket under cursor
	ModeMsg            = {fg = c5, bg =  x}, -- showmode
	MoreMsg            = {fg = c5, bg =  x, bold = 1}, -- screen filled with messages prompt
	None               = {fg =  x, bg =  x},
	NonText            = {fg = c3, bg =  x, bold = 1},
	Normal             = {fg = c5, bg = c0},
	NormalFloat        = {fg = c5, bg =  x},
	NormalNC           = {fg = c5, bg =  x},
	Number             = {fg = cB, bg =  x, bold = 1},
	Operator           = {fg = c5, bg =  x, bold = 1}, -- =, +, and, or, etc.
	Parameter          = {fg = cE},
	Pmenu              = {fg = c5, bg = c1}, -- popup menu
	PmenuSbar          = {fg = c5, bg =  x}, -- popup menu scrollbar track
	PmenuSel           = {fg = c0, bg = c5}, -- popup menu selected item
	PmenuThumb         = {fg = c5, bg = c3}, -- popup menu scrollbar slider
	PreCondit          = {fg = c5, bg =  x},
	PreProc            = {fg = cC, bg =  x},
	Question           = {fg = c5, bg =  x},
	QuickFixLine       = {fg = c5, bg =  x},
	Repeat             = {fg = cA, bg =  x, bold = 1}, -- for, while, etc.
	Search             = {fg =  x, bg = c2, bold = 1, underline = 0}, -- hlsearch results and inccommand
	SignColumn         = {fg = c5, bg =  x},
	Special            = {fg = c5, bg =  x},
	SpecialChar        = {fg = c7, bg =  x}, -- escape chars
	SpecialComment     = {fg = cD, bg =  x, bold = 1, underline = 0}, -- all caps words followed by :
	SpecialKey         = {fg = c3, bg =  x, bold = 1},
	SpellBad           = {undercurl = 1, sp = c8},
	SpellCap           = {undercurl = 1, sp = cD}, -- word that should be capitalized
	SpellLocal         = {undercurl = 1, sp = cB}, -- word from another region
	SpellRare          = {undercurl = 1, sp = cE},
	Statement          = {fg = c5, bg =  x},
	StatusLine         = {fg = c9, bg =  x},
	StatusLineNC       = {fg = c5, bg =  x},
	StatusLineTerm     = {fg = c5, bg =  x},
	StatusLineTermNC   = {fg = c5, bg =  x},
	StorageClass       = {fg = c5, bg =  x, bold = 1},
	String             = {fg = cB, bg =  x},
	Structure          = {fg = c5, bg =  x},
	TabLine            = {fg = c5, bg =  x},
	TabLineFill        = {fg = c5, bg =  x},
	TabLineSel         = {fg = c5, bg =  x},
	Tag                = {fg = c5, bg =  x, bold = 1},
	Title              = {fg = c5, bg =  x, bold = 1},
	Todo               = {fg = cC, bg =  x, bold = 1, underline = 0}, -- todo, hack, warning
	Type               = {fg = cD, bg =  x, underline = 1, italic = 0, sp = c5},
	Typedef            = {fg = c5, bg =  x, italic = 1},
	Underlined         = {fg = c5, bg =  x, underline = 1},
	Visual             = {bg =  x, reverse = 1}, -- selected text in visual mode
	VisualNOS          = {fg = c5, bg =  x},
	WarningMsg         = {fg = cA, bg =  x, bold = 0}, -- fixme, xxx, bug, warning messages in cmdline
	Whitespace         = {fg = c4, bg =  x, bold = 1}, -- trailing space, tab, etc.
	WildMenu           = {fg = c5, bg =  x},
	WinSeparator       = {fg = c2, bg =  x},

	-- TREESITTER:
	-- TSConditional        = {},
	-- TSConstant           = {},
	-- TSEmphasis           = {},
	-- TSException          = {},
	-- TSFloat              = {},
	-- TSFuncMacro          = {},
	-- TSFunction           = {},
	-- TSInclude            = {},
	-- TSKeyword            = {},
	-- TSLabel              = {},
	-- TSLiteral            = {},
	-- TSNone               = {},
	-- TSNumber             = {},
	-- TSOperator           = {},
	-- TSParameterReferenc  = {},
	-- TSPunctDelimiter     = {},
	-- TSRepeat             = {},
	-- TSStrike             = {},
	-- TSString             = {},
	-- TSStringEscape       = {},
	-- TSStringRegex        = {fg = cC, style = "nocombine"},
	-- TSSymbol             = {},
	-- TSTag                = {},
	-- TSTagDelimiter       = {},
	-- TSUnderline          = {},
	-- TSURI                = {},
	commentTSConstant       = {fg = cE},
	commentTSPunctDelimiter = {fg = c3, bold = 1},
	TSConstBuiltin          = {fg = cD, bg =  x, bold = 0, italic = 1},
	TSConstMacro            = {link = "Include"},
	TSConstructor           = {link = "Normal"},
	TSDanger                = {fg = c9, bold = 1},
	TSError                 = {underline = 0, sp = c8},
	TSField                 = {link = "Field"},
	TSFuncBuiltin           = {link = "Builtin"},
	TSKeywordFunction       = {fg = cE, italic = 1},
	TSKeywordOperator       = {fg = cE, bold = 1},
	TSKeywordReturn         = {fg = cA, bold = 1, italic = 1},
	TSMethod                = {fg = cC, bold = 1},
	TSNamespace             = {fg = c5, underline = 1},
	TSNote                  = {fg = cD, bold = 1},
	TSParameter             = {link = "parameter"},
	TSProperty              = {fg = c5, bold = 1},
	TSPunctBracket          = {link = "Bracket"},
	TSPunctSpecial          = {fg = cF},
	TSStringRegex           = {link = "String"},
	TSText                  = {link = "String"},
	TSTitle                 = {link = "String"},
	TSType                  = {fg = c5, italic = 1},
	TSTypeBuiltin           = {fg = c5, nocombine = 1, bold = 1, italic = 1},
	TSVariable              = {fg = c5},
	TSVariableBuiltin       = {fg = cD},
	TSWarning               = {fg = cC, bold = 1},

	-- CUSTOM:
	Header = {fg = cD, bg =  x},
	Indent = {fg = c3, fga = 50},

	-- DIAGNOSTICS:
	DiagnosticError          = {fg = c8, bg = c8, fga = df, bga = db, bold = 0},
	DiagnosticFloatingError  = {fg = c8, bg = c0},
	DiagnosticUnderlineError = {sp = c8},
	StatusDiagE              = {fg = c0, bg = c8, bold = 1},

	DiagnosticWarn           = {fg = cA, bg = cA, fga = df, bga = db, bold = 0},
	DiagnosticFloatingWarn   = {fg = cA, bg = c0},
	DiagnosticUnderlineWarn  = {sp = cA},
	StatusDiagW              = {fg = c0, bg = cA, bold = 1},

	DiagnosticInfo           = {fg = cE, bg = cE, fga = df, bga = db, bold = 0},
	DiagnosticFloatingInfo   = {fg = cE, bg = c0},
	DiagnosticUnderlineInfo  = {sp = cE},
	StatusDiagI              = {fg = c0, bg = cE, bold = 1},

	DiagnosticHint           = {fg = c5, bg = c5, fga = df, bga = db, bold = 0},
	DiagnosticFloatingHint   = {fg = c5, bg = c0},
	DiagnosticUnderlineHint  = {sp = c5},
	StatusDiagH              = {fg = c0, bg = c5, bold = 1},

	StatusDiag               = {fg = c1, bg =  x},

	-- MAN:
	ManFooter         = {fg = c3, bg =  x, bold = 1}, -- footer
	ManHeader         = {fg = c3, bg =  x, bold = 1}, -- header
	ManLowerSentence  = {bg =  x, italic = 1}, -- ?
	ManOptionDesc     = {fg = cE, bg =  x, bold = 1}, -- parameters
	ManReference      = {fg = cA, bg =  x}, -- links to other man pages
	ManSectionHeading = {fg = cB, bg =  x, bold = 1}, -- heading
	ManSentence       = {bg =  x, italic = 1}, -- ?
	ManSubHeading     = {link = "include"}, -- sub heading

	-- HELP:
	helpCommand        = {fg = cC, bg =  x},
	helpExample        = {fg = cB, bg =  x},
	helpHyperTextEntry = {fg = cF, bg =  x, italic = 0, underline = 0},
	helpHyperTextJump  = {fg = cA, bg =  x, underline = 0},
	helpNote           = {bg =  x, bold = 1},
	helpOption         = {fg = cD, bg =  x},
	helpSpecial        = {fg = cE, bg =  x},
	helpUrl            = {fg = cD, bg =  x, bold = 1, italic = 1},
	helpWarning        = {fg = cA, bg =  x, bold = 1, italic = 0},

	-- DESKTOP:
	dtBooleanKey      = {link = "dtStringKey"},
	dtCategoriesKey   = {link = "dtStringKey"},
	dtDelim           = {link = "Operator"},
	dtExec            = {link = "String"},
	dtExecKey         = {link = "dtStringKey"},
	dtGroup           = {link = "Header"},
	dtIconStringKey   = {link = "dtStringKey"},
	dtLocalestring    = {link = "String"},
	dtLocalestringKey = {link = "dtStringKey"},
	dtStringKey       = {fg = cE},
	dtTypeKey         = {link = "dtStringKey"},
	dtVersionKey      = {link = "dtStringKey"},
	dtXExtensionKey   = {link = "dtStringKey"},

	-- CFG:
	CfgOnOff   = {link = "Boolean"},
	CfgParams  = {fg = cA}, -- ?
	CfgSection = {fg = cD},
	CfgValues  = {fg = cC},

	-- DOSINI:
	dosiniHeader = {link = "Header"},
	dosiniLabel  = {fg = cC},
	dosiniValue  = {fg = c5},

	-- BATCH:
	dosbatchImplicit = {link = "Builtin"},
	dosbatchSwitch   = {link = "parameter"},

	-- POWERSHELL:
	ps1Cmdlet   = {link = "Builtin"},
	ps1Function = {link = "TSFunction"},
	ps1Variable = {link = "TSVariable"},

	-- XML:
	xmlAttrib          = {fg = cD},
	xmlAttribPunct     = {link = "Delimiter"},
	xmlEqual           = {link = "Operator"},
	-- xmlProcessing   = {fg = c3, bg =  x},
	xmlDocType         = {bg =  x, bold = 1},
	xmlDocTypeDecl     = {fg = cF},
	xmlProcessingDelim = {fg = cF},
	xmlTag             = {fg = cF},
	xmlTagName         = {fg = cC},

	-- FSTAB:
	fsMountPoint = {fg = cC, bold = 1},

	-- JAVASCRIPT:
	javascriptTSConstructor = {link = "Variable"},

	-- UDEV:
	udevrulesAssignKey = {link = "Builtin"},
	udevrulesDelimiter = {link = "Bracket"},
	udevrulesEnvVar    = {link = "Constant"},
	udevrulesPath      = {link = "Function"},
	udevrulesRuleEq    = {link = "Operator"},
	udevrulesRuleKey   = {link = "Builtin"},
	udevrulesRuleTest  = {link = "Operator"},
	udevrulesVariable  = {link = "Constant"},

	-- PASSWD:
	passwdUIDColon      = {link = "Delimiter"},
	passwdGIDColon      = {link = "Delimiter"},
	passwdGecosColon    = {link = "Delimiter"},
	passwdDirColon      = {link = "Delimiter"},
	passwdShellColon    = {link = "Delimiter"},
	passwdPasswordColon = {link = "Delimiter"},
	passwdShell         = {link = "Function"},
	passwdDir           = {link = "Builtin"},
	passwdAccount       = {link = "normal"},
	passwdGecos         = {link = "parameter"},
	passwdShadow        = {bold = 1},

	-- NORG:
	NeorgTodoItem1          = {fg =  x},
	NeorgUnorderedList1     = {fg = c3, bold = 1},
	NeorgTodoItem1Undone    = {fg = c3},
	NeorgTodoItem1OnHold    = {fg = cD},
	NeorgTodoItem1Done      = {fg = cB},
	NeorgTodoItem1Uncertain = {fg = cE},
	NeorgTodoItem1Urgent    = {fg = cA},
	NeorgTodoItem1Cancelled = {fg = cF},
	NeorgTodoItem1Pending   = {fg = cC},
	NeorgTodoItem1Recurring = {fg = c9},

	-- REGISTRY:
	registryPath    = {fg = cD},
	registrySpecial = {fg = cD},
	registrySubKey  = {link = "bold"},

	-- I3:
	i3ConfigAction              = {fg = cE},
	i3ConfigBindKeyword         = {fg = cD},
	i3ConfigBlock               = {fg = cF},
	i3ConfigBlockKeyword        = {fg = cD},
	i3ConfigFontKeyword         = {fg = cD},
	i3ConfigInitialize          = {fg = cC},
	i3ConfigInitializeKeyword   = {fg = cD},
	i3ConfigNumber              = {link = "number"},
	i3ConfigUnitOr              = {link = "operator"},
	i3ConfigUnit                = {link = "number"},
	i3ConfigVariableAndModifier = {link = "number"},

	-- PYTHON:
	pythonBuiltin       = {link = "builtin"},
	pythonEscape        = {link = "specialchar"},
	pythonStatement     = {link = "parameter"},
	pythonTSType        = {fg = cC, bold = 1},
	pythonTSKeyword     = {fg = cE, bold = 1},
	pythonTSConstructor = {fg = cC, italic = 1, nocombine = 1},

	-- HEALTHCHECK:
	healthSuccess = {fg = cB, bg = c0, bold = 1},
	healthWarning = {fg = cA, bg = c0, bold = 1},
	healthError   = {fg = c8, bg = c0, bold = 1},

	-- RASI:
	rasiTSType = {bold = 1},

	-- YAML:
	yamlTSProperty       = {fg = cC},
	yamlTSField          = {fg = c5, bold = 1},
	-- yamlTSPunctDelimiter = {fg = c5, bold = 1},

	-- HYDRA:
	HydraRed = {fg = c8, bold = 1},

	-- SNIPPETS:
	-- snipCommand = {fg = cC},
	multiSnipText  = {fg = cE},
	placeHolder    = {fg = cA},
	snippet        = {fg = cD},
	tabStop        = {fg = cC, bold = 0},

	-- STARTIFY:
	StartifyBracket   = {link = "Comment"},
	StartifyFile      = {fg = c5, bold = 0},
	StartifyPath      = {fg = c3, bold = 0},
	StartifySlash     = {fg = c3, bold = 0},
	StartifySpecial   = {fg = c3, bold = 1},

	-- INDENTLINE:
	IndentBlanklineChar      = {link = "Indent"},
	IndentBlanklineSpaceChar = {fg = c3},

	-- STARTUPTIME:
	startuptimeHeader        = {fg = c3, bold = 1},
	startuptimeOtherEvent    = {fg = c5},
	startuptimePercent       = {fg = cE},
	startuptimeSourcingEvent = {fg = cD},
	startuptimeStartupKey    = {fg = c5},
	startuptimeStartupValue  = {fg = c5, bold = 1},
	startuptimeTime          = {fg = cC},

	-- CSS:
	cssTSType = {fg = c5},

	cssAtKeyword   = {link = "Keyword"},
	cssMathParens  = {link = "Bracket"},
	cssFunction    = {link = "Function"},
	cssAttrComma   = {link = "Delimiter"},
	cssBraces      = {link = "Bracket"},
	cssColor       = {fg = c4, bold = 1},
	cssCommonAttr  = {link = "Constant"},
	cssHacks       = {fg = c7, bold = 0},
	cssIdentifier  = {fg = c5},
	cssImportant   = {fg = cE},
	cssNoise       = {link = "Delimiter"},
	cssSelectorOp  = {link = "Bracket"},
	cssSelectorOp2 = {link = "Operator"},
	cssVendor      = {fg = c4},
	cssTagname     = {fg = cD, bold = 0},
	cssDefinition  = {fg = c8},
	cssPseudoClassID  = {fg = c9},

	-- LUA:
	luaTSConstructor = {link = "Bracket"},
	-- luaTSConstant    = {fg = cC, bold = 1},

	-- MAKE:
	makePreCondit = {link = "conditional"},
	makeIdent     = {bold = 1},

	-- TELESCOPE:
	TelescopeBorder   = {fg = c3},
	TelescopeMatching = {fg = cD, bold = 1},
	TelescopeTitle    = {fg = c3, bold = 1},
	-- TelescopeSelection = {link = "cursorline"},

	-- SAMBA:
	sambaSection   = {fg = cE},
	sambaBoolean   = {link = "Boolean"},
	sambaKeyword   = {fg = cD, nocombine = 1},
	sambaParameter = {bold = 1},

	-- PACKER:
	packerStatusSuccess = {fg = cB},
	packerSuccess       = {fg = cB},
	packerOutput        = {fg = cE},
	packerHash          = {fg = c3},

	-- HTML:
	TSTagAttribute = {fg = cC},

	-- PUG:
	htmlArg                = {fg = cC},
	htmlTagName            = {fg = cD},
	pugAttributesDelimiter = {link = "Bracket"},
	pugDocType             = {fg = c9},
	pugHtmlArg             = {fg = cC},
	pugTagInlineText       = {link = "String"},
	pugJavascriptString    = {link = "String"},

	-- SASS:
	sassProperty = {link = "Delimiter"},
	sassVariable = {fg = cA},
	sassVariableAssignment = {link = "Delimiter"},
	sassMixin = {link = "TSKeywordFunction"},
	sassMixinName = {link = "Function"},
	sassCssAttribute = {link = "Bracket"},

	TreesitterContext = {bg = c1},

	-- SXKHD:
	sxhkdrcTSKeyword  = {fg = cD},
	sxhkdrcTSType     = {fg = cE},
	sxhkdrcTSVariable = {fg = cC},

	-- STATUSLINE:
	StatusD        = {fg = c5, bg = c1, bold = 1}, -- default
	StatusC        = {fg = c0, bg = c5, bold = 1}, -- command
	StatusC2       = {fg = c5, bg = c0, bold = 1}, -- command deco
	StatusI        = {fg = c0, bg = cB, bold = 1}, -- insert
	StatusI2       = {fg = cB, bg = c0, bold = 1}, -- insert deco
	StatusMod      = {fg = c5, bg = c0, bold = 1, underline = 1, sp = cE}, -- modified
	StatusN        = {fg = c5, bg = c1, bold = 1}, -- normal
	StatusN2       = {fg = c1, bg = c0, bold = 1}, -- normal deco
	StatusR        = {fg = c0, bg = cA, bold = 1}, -- replace
	StatusR2       = {fg = cA, bg = c0, bold = 1}, -- replace deco
	StatusQ        = {fg = c0, bg = c9, bold = 1}, -- interactive replace
	StatusQ2       = {fg = c9, bg = c0, bold = 1}, -- interactive replace deco
	StatusT        = {fg = c0, bg = cE, bold = 1}, -- terminal
	StatusT2       = {fg = cE, bg = c0, bold = 1}, -- terminal deco
	StatusV        = {fg = c0, bg = cD, bold = 1}, -- visual
	StatusV2       = {fg = cD, bg = c0, bold = 1}, -- visual deco
	StatusS        = {fg = c0, bg = cC, bold = 1},
	StatusS2       = {fg = cC, bg = c0, bold = 1},
	StatusB        = {fg = c0, bg = cF, bold = 1},
	StatusB2       = {fg = cF, bg = c0, bold = 1},
	StatusUnknown  = {fg = c0, bg = c8, bold = 1}, -- select
	StatusUnknown2 = {fg = c8, bg = c0, bold = 1}, -- select deco
	StatusDir      = {fg = c4, bg =  x, bold = 0},
	StatusDir2     = {fg = c4, bg =  x, bold = 0, underline = 1, sp = cE},
	StatusSearch   = {fg = c3, bg =  x, bold = 1},
}

-- local gamma = 2.2
local function base(color)
	return {
		tonumber(color:sub(2, 3), 16),
		tonumber(color:sub(4, 5), 16),
		tonumber(color:sub(6, 7), 16),
	}
end
local function blend(color, color2, alpha)
	local background
	if color2 ~= nil then
		background = base(color2)
	else
		background = base(c0)
	end
	alpha = alpha / 100
	color = color:sub(2)
	local output = "#"
	for i=2, 6, 2 do
		local part = tonumber(color:sub(i - 1, i), 16)
		part = math.floor((1 - alpha) * background[i / 2] + (part * alpha))
		-- part = math.floor((base[i/2]^gamma * (1 - alpha) + part^gamma * alpha) ^ (1 / gamma))
		output = output .. string.format("%02X", part)
	end
	return output
end

local api = vim.api
for name, settings in pairs(theme) do -- apply theme
	if settings.fga or settings.fg2 then -- apply alpha / opacity
		settings.fg = blend(settings.fg, settings.fg2, settings.fga)
		settings.fga = nil
		settings.fg2 = nil
	end
	if settings.bga or settings.bg2 then -- apply alpha / opacity
		settings.bg = blend(settings.bg, settings.bg2, settings.bga)
		settings.bga = nil
		settings.bg2 = nil
	end
	-- vim.api.nvim_set_hl(0, name, theme["None"])
	api.nvim_set_hl(0, name, settings)
end
