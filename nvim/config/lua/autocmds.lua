local api = vim.api
api.nvim_create_augroup("all", { clear = true })
function Autocmd(events, pat, cmd, callback)
    if callback == nil then
        api.nvim_create_autocmd(events, { pattern = pat, command = cmd, group = "all" })
    else
        api.nvim_create_autocmd(events, { pattern = pat, callback = callback, group = "all" })
    end
end

-- autocmd("BufEnter", "*", "lua StatusActive()")
-- autocmd("TermLeave", "*", "lua StatusActive()")
-- autocmd("WinEnter", "*", "lua StatusActive()")
-- autocmd("WinLeave", "*", "lua StatusInactive()")
-- autocmd(FileType i3config call timer_start(0, {tid -> execute('so ~/.config/nvim/lua/theme.lua')})
Autocmd("BufEnter", "*", "setlocal cursorline")
Autocmd("BufLeave", "*", "setlocal nocursorline")
Autocmd("BufWritePost", "*config/kitty/kitty.conf", ":silent !kill -USR1 $(pgrep kitty)") -- refresh kitty on config save
Autocmd("BufWritePost", "*.pug", ":silent !pypugjs % %:r.html") -- convert pug to html on save
Autocmd("BufWritePost", {"*.sass", "*.scss"}, ":silent !sassc -t expanded % %:r.css") -- convert sass to css on save
Autocmd("FileType", "*", "set formatoptions+=r")
Autocmd("FileType", "*", "set formatoptions-=o")
Autocmd("ModeChanged", "*:i", "setlocal nolist")
Autocmd("ModeChanged", "i:*", "setlocal list")
Autocmd("TextYankPost", "*", "silent! lua vim.highlight.on_yank{higroup='visual', timeout='500'}")
Autocmd("User", "Startified", "setlocal cursorline")
Autocmd("VimResized", "*", "wincmd =")

-- local filetype = vim.filetype.add -- TODO when filetype.lua is default
Autocmd("BufRead,BufNewFile", "*.bbmodel", "setf json")
Autocmd("BufRead,BufNewFile", "*.conf", "setf conf")
Autocmd("BufRead,BufNewFile", "*.glsl", "setf glsl")
Autocmd("BufRead,BufNewFile", "*.inf", "setf dosini")
Autocmd("BufRead,BufNewFile", "*.tmp", "setf dosini")
Autocmd("BufRead,BufNewFile", "*.pkgbuild", "setf sh")
Autocmd("BufRead,BufNewFile", "*.rasi", "setf rasi")
Autocmd("BufRead,BufNewFile", "*.reg", "setf registry")
Autocmd("BufRead,BufNewFile", "*.rules", "setf udevrules")
Autocmd("BufRead,BufNewFile", "*.theme", "setf conf")
Autocmd("BufRead,BufNewFile", "*.theme", "setf dosini")
Autocmd("BufRead,BufNewFile", "*.trashinfo", "setf dosini")
Autocmd("BufRead,BufNewFile", "*.txt", "setlocal wrap linebreak")
Autocmd("BufRead,BufNewFile", "*.vifm", "setf vim")
Autocmd("BufRead,BufNewFile", "*.yml", "setf yaml")
Autocmd("BufRead,BufNewFile", "*fontconfig/*", "set ft=xml")
Autocmd("BufRead,BufNewFile", "/tmp/*", "setlocal noundofile")
Autocmd("BufRead,BufNewFile", "fish_variables", "setf fish")

local function indent_on()
    local ft = api.nvim_buf_get_option(0, "filetype")
    for _, i in pairs(vim.g.indent_blankline_filetype_exclude) do
        if i == ft then return end -- ft in exclude list
    end
    vim.cmd("IndentBlanklineEnable") -- ft not in exclude list
end
Autocmd("ModeChanged", "*:n", "", indent_on)
Autocmd("ModeChanged", "n:[vV]", "IndentBlanklineDisable")

-- TODO make this (save cursor pos) global?
local function trail()
	local pos = api.nvim_win_get_cursor(0)
	vim.cmd("%s/\\s\\+$//e")
	api.nvim_win_set_cursor(0, pos)
end
Autocmd("BufWritePre", "*", "", trail) -- remove trailing whitespace on save
-- TODO toggle diags in snippets
-- vim.api.nvim_create_autocmd("ModeChanged", {
--   group    = augroup,
--   pattern  = "*:s",
--   callback = function ()
--     if luasnip.in_snippet() then
--       return vim.diagnostic.disable()
--     end
--   end
-- })

-- local status = require("statusline-again")
-- Autocmd({"BufEnter", "BufModifiedSet", "ModeChanged"}, "", "", status.Update)
-- -- Autocmd("BufEnter", "*", "", status.BufEnter)
-- -- Autocmd("BufModifiedSet", "*", "", status.BufModifiedSet)
-- Autocmd("CursorMoved", "*", "", status.Update)
-- -- Autocmd("ModeChanged", "*:*", "", status.ModeChanged)
-- -- Autocmd({"TextChanged", "TextChangedI"}, "*", "", status.TextChanged)
