-- install packer if needed{{{
local fn = vim.fn
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	print("Installing Packer...")
	Packer_bootstrap = fn.system {
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	}
	print("Packer installed. Restart Neovim.")
end

-- return if packer can"t be loaded
local loaded, packer = pcall(require, "packer")
if not loaded then
	return
end

-- display in a floating window instead of a split
packer.init {
	display = {
		open_fn = function()
			return require("packer.util").float {border = "rounded"}
		end
	}
}
--}}}

-- PLUGINS:
require("packer").startup(function()
	local use = packer.use
	use { -- plugin manager
		"https://github.com/wbthomason/packer.nvim",
	}
	use { -- required for some other plugins
		"https://github.com/nvim-lua/plenary.nvim",
		config = function() require('plenary.filetype').add_file("plug/plenary") end,
	}
	use { -- highlighting and other stuff
		"https://github.com/nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
		config = function() require("plug/treesitter") end,
	}
	use { -- display treesitter groups, etc.
		"https://github.com/nvim-treesitter/playground",
	}
	use { -- commenting
		"https://github.com/numToStr/Comment.nvim",
		config = function() require("Comment").setup() end,
	}
	use { -- colorizes hex codes TODO replace with nvchad version?
		"https://github.com/norcalli/nvim-colorizer.lua",
		config = function() require("plug/colorizer") end,
	}
	-- use { -- start screen TODO replace with dashboard or alpha
	-- 	"https://github.com/mhinz/vim-startify",
	-- 	config = function() require("plug/startify") end,
	-- }
	use { -- vertical indent lines
		"https://github.com/lukas-reineke/indent-blankline.nvim",
		config = function() require("plug/indentline") end,
	}
	use { -- display how long nvim takes to start
		"https://github.com/dstein64/vim-startuptime",
	}
	use { -- surround text
		"https://github.com/kylechui/nvim-surround",
		config = function() require("plug/surround") end,
	}
	use { -- auto add closing bracket, etc.
		"https://github.com/windwp/nvim-autopairs",
		config = function() require("plug/autopairs") end,
	}
	-- use { -- fuzzy finder
	-- 	"https://github.com/ibhagwan/fzf-lua",
	-- 	config = function() require("plug/fzf") end,
	-- }
	use { -- align text
		"https://github.com/junegunn/vim-easy-align",
		config = function() require("plug/easy-align") end,
	}
	use { -- install language servers
		"https://github.com/williamboman/nvim-lsp-installer",
		-- config = function() require("plug/lspinstaller") end,
	}
	use { -- configure language servers
		"https://github.com/neovim/nvim-lspconfig",
		config = function() require("plug/lsp-config") end,
		-- after = "nvim-lsp-installer",
	}
	-- use { -- add more modes
	-- 	"https://github.com/anuvyklack/hydra.nvim",
	-- 	config = function() require("plug/hydra") end,
	-- }
	use {
		"https://github.com/nvim-telescope/telescope-fzf-native.nvim",
		run = "make"
	}
	use { -- TODO customize
		"https://github.com/nvim-telescope/telescope.nvim",
		tag = "0.1.*",
		config = function() require("plug/telescope") end,
		-- requires = {
		-- }
	}
	use { -- TODO add more snippets
		"https://github.com/L3MON4D3/LuaSnip",
		config = function() require("plug/snip") end,
	}
	use {
		"https://github.com/digitaltoad/vim-pug"
	}
	-- use { -- show current scope at top
	-- 	"https://github.com/nvim-treesitter/nvim-treesitter-context",
	-- 	config = function() require("plug/context") end,
	-- }
	-- use {
	-- 	"https://github.com/glacambre/firenvim",
	-- 	run = function() fn['firenvim#install'](0) end
	-- }

	-- TODO rainbow brackets?
	-- https://github.com/p00f/nvim-ts-rainbow

	-- use "akinsho/bufferline.nvim" -- TODO replace bufcount with this?
	-- use "https://github.com/tpope/vim-surround"
	-- use "Vonr/align.nvim"
	-- use "https://github.com/glepnir/dashboard-nvim"

	-- use {
	--	 "nvim-neorg/neorg",
	--	 ft = "norg",
	--	 config = function()
	--		 require("plug/neorg")
	--	 end,
	--	 requires = "nvim-lua/plenary.nvim",
	-- }
	-- use {
	--	 "nvim-telescope/telescope.nvim",
	--	 config = function()
	--		 require("plug/telescope")
	--	 end,
	--	 -- requires = "nvim-telescope/telescope-fzy-native.nvim"
	-- }
	-- use {
	--	 "nvim-telescope/telescope-fzf-native.nvim",
	--	 run = "make",
	-- }
	-- use "mboughaba/i3config.vim"
	-- use "kmonad/kmonad-vim"

	if Packer_bootstrap then
		require("packer").sync()
	end
end)
