#!/bin/python -u
from basic import date, expand
from subp import run, cmd
import os


def refresh(fd):
    fd.seek(0)
    fd.truncate()
    fd.write(f"{date('%s')}\n")
    print("?")
    updates = cmd("checkupdates")
    text = ""
    if len(updates) > 0:
        u_list = updates.split("\n")
        print(len(u_list) - 1)
        for line in u_list:
            for n, col in enumerate(line.split()):  # add color to specific parts
                if n == 0:
                    if col in (
                        "bash",
                        "dash",
                        "fish",
                        "coreutils",
                        "grub",
                    ) or col.startswith(("linux", "nvidia", "systemd")):
                        text += f"\033[35m{col}\033[0m "  # ]]
                    else:
                        text += f"{col} "
                elif n == 1:
                    text += f"\033[34m{col}\033[0m "  # ]]
                elif n == 2:
                    text += f"{col} "
                elif n == 3:
                    text += f"\033[36m{col}\033[0m\n"  # ]]
    else:
        print(f"%{{F{os.environ['c3_h']}}}0")
    fd.write(text)


def check(msg):
    file = "/tmp/updates"
    fd = open(file, "a+")
    fd.seek(0)
    if msg == "force":
        refresh(fd)
        return
    try:
        time = int(fd.readline())
        if int(date("%s")) - time >= 3_600:
            refresh(fd)
        else:
            run(f"dunstify -t 2000 -r 1 '{3_600 - (int(date('%s')) - time)}s left'")
            num = len(fd.read().split("\n")) - 1
            if num > 0:
                print(num)
            else:
                print(f"%{{F{os.environ['c3_h']}}}0")
    except ValueError:
        refresh(fd)


def main():
    fifo = expand("~/scripts/fifo/updates")
    if not os.path.exists(fifo):
        os.mkfifo(fifo)

    check("")
    while True:
        check(open(fifo).read())


if __name__ == "__main__":
    main()
