#!/bin/python -u
from basic import *
from subp import *
import os

def check_idle(timer):
    while True:
        idle = int(cmd("xprintidle").strip("\n")) / 1000
        if timer > idle:
            wait = timer - idle
            print(f"sleeping for {wait}s")
            sleep(wait)
        else:
            break


def activate():
    print("active")
    sleep(1)
    # with cmdi...


def main():
    timer = 3
    feh   = "feh -ZF"
    fifo  = expand("~/scripts/fifo/screensaver")

    if not os.path.exists(fifo):
        os.mkfifo(fifo)

    while True:
        check_idle(timer)
        activate()


if __name__ == "__main__":
    main()
