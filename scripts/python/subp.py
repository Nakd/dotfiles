# import subprocess
from subprocess import (
        run as Run,
        Popen,
        PIPE,
        STDOUT
        )
from os.path import expanduser as expand


def run(c):  # run command
    return Run(expand(c), shell=True)


def runbg(c):  # run command in background
    return Popen(expand(c), shell=True)


def cmd(c):  # get output of command
    command = Run(
        expand(c),
        shell=True,
        stdout=PIPE,
        stderr=STDOUT,
        text=True,
    )
    return command.stdout


def cmdi(c):  # get output of continuous command
    return Popen(expand(c), shell=True, stdout=PIPE, text=True).stdout
