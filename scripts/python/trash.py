#!/bin/python
import os
import shutil

basename = os.path.basename
dirname = os.path.dirname
exists = os.path.lexists
expand = os.path.expanduser
join = os.path.join
home = expand("~")
if home == "/root":
    exit(1)

try:
    fs = os.environ["fs"]
except KeyError:
    fs = None

t_dir = expand("~/.local/share/Trash")
f_dir = join(t_dir, "files")
i_dir = join(t_dir, "info")
err = ""

for d in (t_dir, f_dir, i_dir):
    if not exists(d):
        os.makedirs(d)


def wrap(func):
    def a(*args):
        global err
        err = ""
        if args:
            args = args[0]
        args = [str(i) for i in args]
        func(args)
        return err.rstrip("\n")

    return a


def check(name):
    file = join(f_dir, name)
    i = 1

    # First do an exponential search
    while os.path.exists(f"{file}_{i}"):
        i = i * 2

    # Result lies somewhere in the interval (i/2..i]
    # We call this interval (a..b] and narrow it down until a + 1 = b
    a, b = (i // 2, i)
    while a + 1 < b:
        c = (a + b) // 2  # interval midpoint
        a, b = (c, b) if os.path.exists(f"{file}_{c}") else (a, c)
    return f"{name}_{b}"


@wrap
def move(files):
    # maybe save time when file was trashed in info file?
    for f in files:
        name = basename(f)
        f = join(os.getcwd(), name)
        if not exists(f):
            msg(f"'{f}' doesn't exist")
            continue
        if f.startswith(t_dir):
            msg(f"'{name}' is already in the trash directory")
            continue
        f_file = join(f_dir, name)
        if exists(f_file):
            name = check(name)
            f_file = join(f_dir, name)
        try:
            shutil.move(f, f_file)
            with open(join(i_dir, name), "w") as fd:
                fd.write(f)
        except Exception as e:
            msg(e)


@wrap
def restore(files):
    # accepts f_dir relative name (full dir would require checking every info file)
    for f in files:
        name = basename(f)
        f_file = join(f_dir, name)
        f_info = join(i_dir, name)
        if not exists(f_file):
            msg(f"'{name}' does not exist in the trash directory")
            continue
        if not exists(f_info):
            msg(f"'{name}' info file does not exist in the trash directory")
            continue
        with open(f_info) as fd:
            path = fd.read()
        if exists(path):
            msg(f"{name} already exists at the original location")
            continue
        try:
            shutil.move(f_file, path)
            os.remove(f_info)
        except Exception as e:
            msg(e)


@wrap
def delete(files):
    for f in files:
        name = basename(f)
        f_file = join(f_dir, name)
        f_info = join(i_dir, name)
        try:
            os.remove(f_info)
            os.remove(f_file)
        except IsADirectoryError:
            shutil.rmtree(f_file)
        except FileNotFoundError:
            msg(f"'{name}' does not exist in the trash directory")


@wrap
def empty(*args):
    orig = os.getcwd()

    os.chdir(i_dir)
    for i in os.listdir(i_dir):
        try:
            if os.path.isfile(i) or os.path.islink(i):
                os.remove(i)
            else:
                shutil.rmtree(i)
        except Exception as e:
            msg(e)

    os.chdir(f_dir)
    for f in os.listdir(f_dir):
        try:
            if os.path.isfile(f) or os.path.islink(f):
                os.remove(f)
            else:
                shutil.rmtree(f)
        except Exception as e:
            msg(e)

    os.chdir(orig)


def msg(x):
    if __name__ == "__main__":
        print(x)
    else:
        global err
        err += x + "\n"


def usage():
    from inspect import cleandoc

    print(
        cleandoc(
            """
            Usage: trash [options]

            Options:
                -h, --help             show this help message and exit
                <files>                trash the specified files
                -- <files>             treat rest of args as files and trash them
                -r, --restore <files>  restore the specified files from the trash
                -d, --delete <files>   delete the specified files in the trash
                --empty                delete all files in the trash
            """
        )
    )
    exit()


def main():
    from sys import argv as args

    for a in args:
        if len(a) == 0:
            args.remove(a)
    usage() if len(args) == 1 else args.pop(0)

    if fs in args:
        args.remove(fs)
        args += fs.split("\n")

    match args[0]:
        case "-h" | "--help":
            usage()
        case "-d" | "--delete":
            usage() if len(args) == 1 else args.pop(0)
            delete(args)
        case "-r" | "--restore":
            usage() if len(args) == 1 else args.pop(0)
            restore(args)
        case "--":
            usage() if len(args) == 1 else args.pop(0)
            move(args)
        case "--empty":
            if len(args) > 1:
                print("warning: ignoring extra args")
            empty()
        case _:
            move(args)


if __name__ == "__main__":
    main()
