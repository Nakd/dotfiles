#!/bin/python
from os.path import expanduser as expand
from os import environ
from time import (
    strftime as date,
    perf_counter_ns as t,
    sleep,
)

home = expand("~")


def env(i):  # get value of env var
    return environ.get(i)


def tt(i):  # total time
    return (t() - i) / 1e9


# sB     = "\033[1m"
# sU     = "\033[4m"
# # sS   = '\033[8m'
# sN     = "\033[0m"
# black  = "\033[30m"
# red    = "\033[31m"
# green  = "\033[32m"
# yellow = "\033[33m"
# blue   = "\033[34m"
# purple = "\033[35m"
# cyan   = "\033[36m"
# white  = "\033[37m"
# gray   = "\033[90m"
# # c9   = '\033[91m'
# orange = "\033[92m"
# brown  = "\033[93m"
# # cC   = '\033[94m'
# # cD   = '\033[95m'
# # cE   = '\033[96m'
# bright = "\033[97m"
