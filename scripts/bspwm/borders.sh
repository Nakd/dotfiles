#!/bin/sh
bw=1
IFS='
'
on="bspc config border_width $bw"
off="bspc config border_width 0"

bspc subscribe node_add node_remove node_state desktop_layout desktop_focus | while read -r line; do

    # echo "$line"

    case $line in
        *"floating on"*)
            eval $on
            continue
            ;;
    esac

    list=$(bspc query -N -n .window -d focused)
    count=0
    for _ in $list; do # get window count
        count=$((count + 1))
    done

    if [ "$count" -gt 1 ]; then # show borders if there are 2+ windows
        case $(bspc wm -g) in
            *LT*) eval $on;;
            *LM*)
                case $(bspc query -T -d focused) in
                    *'"state":"floating"'*) eval $on;;
                    *) eval $off;; # unless in monocle mode
                esac
        esac
    else
        eval $off
    fi
done
