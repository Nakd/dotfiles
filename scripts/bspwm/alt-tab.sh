#!/bin/sh -x
IFS="
"
count=0
state=$(bspc wm -g)
windows=$(bspc query -N -d focused -n .window)

for i in $windows; do
    count=$((count + 1))
done

case $state in
    *LT*) bspc desktop -f next.local;;
    *LM*)
        if [ "$count" -gt 1 ]; then
            bspc node -f next.local.!hidden.window
            count=$((count - 1))
        else
            bspc desktop -f next.local
        fi
        ;;
esac
