#!/bin/python -u
from basic import *
import os
import shutil

try:
    sep = env('lf_filesep')
    files = env('fx').split(sep)
except:
    print('no file(s) selected')
    exit(1)
opts = {}
# config = open(f'{home}/dots/install.yaml', 'a+')
# config.seek(0)
# text = config.read()
# config_root = open(f'{home}/dots/install-root.yaml', 'a+')
# config_root.seek(0)
# text_root = config_root.read()

def local(src, dst):
    if not os.path.exists(dst):
        shutil.move(src, dst)

# def root():

for file in files:
    if os.path.islink(file):
        print(f'skipping symlink {cyan}{file}{norm}')
        continue
    name = os.path.basename(file)
    dirname = os.path.dirname(file)
    isfile = os.path.isfile(file)
    print(f'{gray}name: {norm}' + name)
    print(f'{gray}dir:  {norm}' + dirname)

    if isfile:
        if dirname == home:
            opt_dst = '~/'
            opt_src = 'home'
        elif home in dirname:
            opt_dst = f'~/{dirname[len(home) + 1:]}'
            opt_src = os.path.basename(opt_dst)
        else:
            opt_dst = dirname
            opt_src = os.path.basename(opt_dst)
    else:
        if dirname == home:
            opt_dst = f'~/{name}'
            opt_src = 'home'
        elif home in dirname:
            opt_dst = f'~/{dirname[len(home) + 1:]}/{name}'
            opt_src = os.path.basename(opt_dst)
        else:
            opt_dst = file
            opt_src = os.path.basename(opt_dst)
    print(f'{gray}dst:  {norm}' + opt_dst)
    print(f'{gray}src:  {norm}' + opt_src)
    print(f'{gray}test: {norm}{opt_dst}: {opt_src}/**')
    continue
    # else:
    #
    # if isfile and dirname == home:
    #     opt_dir = '~/'
    #     opt_name = 'home'
    # elif not isfile and dirname == home:
    #     opt_dir = f'~/{name}'
    # elif isfile and home in dirname:
    #     opt_dir = f'~/{dirname[len(home) + 1:]}'
    # elif not isfile and home in dirname:
    #     opt_dir = f'~/{dirname[len(home) + 1:]}/{name}'
    #
    # # not in home dir
    # elif isfile:
    #     dir = dirname
    # else:
    #     dir = file

    if '~/dots' not in dir and dir not in text and dir not in text_root:
        print(f'{blue}{bold}{file}\033[0m')
        opts[1] = f'{dir}:'
        opts[2] = f'{dir}: {name}/*'
        opts[3] = f'{dir}: {name}/**'
        opts[4] = '\033[36mcustom\033[0m'
        print()
        for i in opts:
            print(f'{i}) {opts[i]}')
        print()
        while True:
            try:
                choice = int(input('selection: '))
                line = opts[choice]
                if choice == 4:
                    line = input('custom line: ')
                choice = input(f'Is "\033[36;1m{line}\033[0m" okay? [Y/n] ')
                # if choice in ('', 'Y', 'y'):
                #     if isfile:
                #         dst = {}
                #     else:
                #     print(file)
                #     print(dst)
                    # if not isroot:
                    #     local(file, )
                    # mkdir, append
            except (KeyError, ValueError):
                True
            except (KeyboardInterrupt, EOFError):
                print()
                exit(130)
            else:
                break
    else:
        print(f'\033[36m{dir}\033[0m already in config')
