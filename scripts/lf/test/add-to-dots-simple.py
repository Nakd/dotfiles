#!/bin/python -u
# TODO auto dst, input src
from basic import *
import os
import shutil

# try:
    # sep = env('lf_filesep')
    # files = env('fx').split(sep)
file = env('f')
if file == None:
    print('no file selected')
    exit(1)
config = open(f'{home}/dots/install.yaml', 'a+')
config.seek(0)
text = config.read()
# config_root = open(f'{home}/dots/install-root.yaml', 'a+')
# config_root.seek(0)
# text_root = config_root.read()

def move(src, dst):
    if not os.path.exists(f'{dst}/{name}'):
        print(f'{green}moving {src} to {dst}')
        shutil.move(src, dst)
    else:
        print(f'{dst} already exists')

def tilde(i):
    return f'~/{i[len(home) + 1:]}'

name = os.path.basename(file)
dirname = os.path.dirname(file)
isfile = os.path.isfile(file)
homelen = file[len(home) + 1:]
basedir = os.path.basename(dirname)
if os.path.islink(file) or home not in file:
    print('invalid path')
    exit(1)

if isfile:
    conf_dst = tilde(dirname)
    conf_src = f'{basedir}'
    deco = f"{sB}"
    if dirname == home:
        conf_src = 'home'
else:
    conf_dst = tilde(file)
    conf_src = f'{name}'
    deco = f"{sB}{blue}"

dst = f'{home}/dots/{conf_src}'

# if isfile and dirname == home:
#     conf_src = 'home/?'
#     dst = dirname
# elif isfile and dirname != home:
#     conf_src = f'{basedir}/?'
#     dst = dirname
# elif not isfile and dirname == home:
#     conf_src = f'{name}/?'
#     dst = dirname
# elif not isfile and dirname != home:
#     conf_src = f'{name}/?'
#     dst = dirname
# else:
#     dst = name

print(deco + file + sN)
print()
# print(conf_dst)
print(f'{sN}{sB}{conf_dst}{brown}:{sN} {cyan}{conf_src}{sN}')
if not f'{conf_dst}:' in text:
    print('hi')

print(f'moving {deco}{file}{sN}\nto {sB}--> {blue}{dst}{sN}')
try:
    choice = input('Okay? ')
except (KeyboardInterrupt, EOFError):
    print()
    exit(130)
if choice in ('Y', 'y', ''):
    # CAUTION:
    move(file, dst)

# def root():

# for file in files:
#     if os.path.islink(file):
#         print(f'skipping symlink {cyan}{file}{norm}')
#         files.remove(file)
#         continue
# print(len(files))
# # if len(files) == 1:
# #     ?????
# dst_dir = os.path.dirname(files[0])
# print(dst_dir)
# exit()
    # inhome = home in file
    # while True:
    #     try:
    #         line = input(f'{bold}add line to install.yaml: {norm}').split(': ')
    #         print(line)
    #         # print(type(line[0]))
    #         # exit()
    #     # if choice in ('', 'Y', 'y'):
    #         if inhome:
    #             local(line[0], line[1])
    #         # else:
    #         #     root(src, dst)
    #     except IndexError:
    #         print(f'{yellow}required format{norm} dst: src')
    #     # except (KeyError, ValueError):
    #     #     True
    #     except (KeyboardInterrupt, EOFError):
    #         print()
    #         exit(130)
    #     else:
    #         break
