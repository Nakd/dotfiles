#!/bin/python -u
from basic import env, expand
import os

sep = env("lf_filesep")
list = env("fx").split(sep)
dir = expand("~/misc/dots-public")

for i in list:
    print(f"{i} {dir}/{os.path.basename(i)} none bind")
