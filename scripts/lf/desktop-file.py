#!/bin/python
import os
from inspect import cleandoc
from subp import cmd
import shutil


def largest(items):
    # files in dir, files in 7z list
    if items:
        pass  # ?


def main():
    file = os.environ["f"]
    base = os.path.basename(file)
    types = (".exe", ".appimage")
    name = None
    for t in types:
        if file.lower().endswith(t):
            name = base[:-len(t)]
            type = t
            break
    if name is None:
        print("\033[33mnot a valid file\033[0m")  # ]]
        exit(1)
    desktop_file = os.path.expanduser(f"~/.local/share/applications/{name}.desktop")
    icon_dir = os.path.expanduser("~/Games/.icons")

    icon = f"{base[:-len(type)]}.ico"
    tmp_dir = "/tmp/icon"
    os.makedirs(tmp_dir, exist_ok=True)
    os.chdir(tmp_dir)
    print(cmd(f"icoextract '{file}' '{icon}'"), end="")
    if True:  # TODO get exit code of icoextract
        print(cmd(f"convert {icon} {name}.png"), end="")
        size = 0
        tmp_name = f"{name}.png"
        for i in os.listdir(tmp_dir):
            cur_size = os.stat(i).st_size
            if cur_size > size and i.endswith(".png"):
                size = cur_size
                largest_icon = i
        shutil.move(largest_icon, tmp_name)  # rename
        final_icon = os.path.join(icon_dir, tmp_name)
        if os.path.isfile(final_icon):
            os.remove(final_icon)
        shutil.move(tmp_name, icon_dir)
        shutil.rmtree(tmp_dir)
    else:
        # TODO if icoextract fails, try 7z
        line_size = 0
        for line in cmd(f"7z l {file}").split("\n"):
            line_l = line.lower()
            print(line)
            if "icon" in line_l and line_l.endswith((".ico", ".png")):
                line_list = line.split()
                line_size_cur = int(line_list[1])
                if line_size_cur > line_size:
                    line_size = line_size_cur
                    line_largest = " ".join(line_list[3:])  # get icon path
        if line_size > 0:
            if line_largest.lower().endswith(".png"):
                pass  # use this image
            elif line_largest.lower().endswith(".ico"):
                pass  # convert this image
            print(line_largest)
        else:
            final_icon = "itmages-question"
        exit()

    ans = input(f"Should \033[34m{base}\033[0m have internet access? (y/N) ")  # ]]
    net = "i" if ans in ("y", "Y") else ""
    content = cleandoc(
        f"""
        [Desktop Entry]
        Exec=box -g{net} '{file}'
        Name={name}
        Type=Application
        Icon={final_icon}
        """
    )
    with open(desktop_file, "w") as f:
        f.truncate()
        f.write(content)


if __name__ == "__main__":
    main()
