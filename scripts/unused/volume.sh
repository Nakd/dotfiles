#!/bin/sh

icon=~/scripts/fifo/volume-icon
fifo=~/scripts/fifo/volume
n="dunstify -t 1500 -r 1 --"
[ -p "$fifo" ] || mkfifo "$fifo"
volF='/tmp/sound/vol'
muteF='/tmp/sound/mute'
modeF='/tmp/sound/mode'

while true; do
    while read -r change; do

        read -r vol < "$volF"
        read -r mute < "$muteF"
        read -r mode < "$modeF"

        echo "$vol"

        case $mode in
            0) M="•";;
            1) M="*";;
        esac
        case $mute in
            1)
                m1="<span color='$c3'>"
                m2="</span>"
                ;;
            0)
                m1=""
                m2=""
                ;;
        esac
        case $change in
            up)
                # c="+"
                pamixer -i 2
                if [ "$vol" -lt 100 ]; then
                    vol=$((vol + 2))
                    printf "%s" "$vol" > "$volF"
                fi
                ;;
            down)
                # c="-"
                pamixer -d 2
                if [ "$vol" -gt 0 ]; then
                    vol=$((vol - 2))
                    printf "%s" "$vol" > "$volF"
                fi
                ;;
            mute)
                # c="/"
                pamixer -t
                case $mute in
                    1)
                        printf "0" > "$muteF"
                        printf "%s\n" "$M" > "$icon"
                        m1=""
                        m2=""
                        ;;
                    0)
                        printf "1" > "$muteF"
                        printf "%s\n" "$M" > "$icon"
                        m1="<span color='$c3'>"
                        m2="</span>"
                        ;;
                esac
                ;;
            mode)
                case $mode in
                    0)
                        v1="$vol"
                        if [ -n "$v2" ]; then
                            pamixer --set-volume "$v2"
                            printf "%s" "$v2" > "$volF"
                            vol="$v2"
                        fi
                        easyeffects -l eq-limit
                        M="*"
                        printf "%s\n" "$M" > "$icon"
                        printf '1' > "$modeF"
                        ;;
                    1)
                        v2="$vol"
                        if [ -n "$v1" ]; then
                            pamixer --set-volume "$v1"
                            printf "%s" "$v1" > "$volF"
                            vol="$v1"
                        fi
                        easyeffects -l eq
                        M="•"
                        printf "%s\n" "$M" > "$icon"
                        printf '0' > "$modeF"
                        ;;
                esac
                ;;
        esac
        nm="$m1$M$vol$m2"
        $n "" "$nm"
    done < "$fifo"
done
