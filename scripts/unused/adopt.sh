#!/bin/sh
cd ~/dotfiles || exit
printf "\033[33mCAUTION\033[0m\n\nPreview of adopted files:\n"
stow --adopt -nvt / *
printf "\nAccept? [y/N] "
read -r response
if [ "$response" = y ]
then
	stow --adopt -vt / * || doas stow --adopt -vt / *
fi
printf "\nPress <enter> to exit."
read -r _
