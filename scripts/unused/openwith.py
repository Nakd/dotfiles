#!/bin/python
from basic import *
import os
import re

home = env('HOME')

app_l = cmd('fd -uue desktop --max-depth 1 --strip-cwd-prefix --base-directory ~/.local/share/applications .')
app_u = cmd('fd -uue desktop --max-depth 1 --strip-cwd-prefix --base-directory /usr/share/applications .')
apps = (app_l + app_u).split()
applist = list(dict.fromkeys(apps))

appstr = ''
for i in applist:
    appstr += i[:-8] + '\n'

result = cmd(f'printf "{appstr}" | fzf')[:-1]
if len(result) == 0:
    exit()

loc1 = f'{home}/.local/share/applications/{result}.desktop'
loc2 = f'/usr/share/applications/{result}.desktop'
if os.path.isfile(loc1):
    loc = loc1
    # print('\033[32m' + loc1 + '\033[0m')
elif os.path.isfile(loc2):
    loc = loc2
    # print('\033[36m' + loc2 + '\033[0m')

with open(loc) as file:
    text = file.read()
# m = re.search(r'[Ee]xec', text)
exec = re.search(r'[Ee]xec.*', text).group()[5:]
run(exec)
exit()
