#!/bin/sh
set -eu

if ! [ $# -eq 1 ]; then
    echo "Expected 1 argument, found $#" >&2
    exit 1
fi

TEMPDIR=$(mktemp -d)
finish () {
    rm -rf "$TEMPDIR"
}
trap finish EXIT

TEMPFILE="$TEMPDIR/$(basename "$1")"
doas cp "$1" "$TEMPDIR"
doas chown "$USER" "$TEMPDIR" "$TEMPFILE"
$EDITOR "$TEMPFILE"
printf "===\n"
printf "$TEMPFILE\n"
printf "===\n"
printf "Are you sure you want to copy the above file to %s? Only 'y' is accepted: " "$1"
read -r REPLY
if [ "$REPLY" = "y" ]; then
    doas cp "$TEMPFILE" "$1"
else
    echo "Received a reply that is not 'yes'. Not moving the edited file."
    exit 1
fi
