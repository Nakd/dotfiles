#!/bin/sh
if [ -f "/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input" ]; then
    temp=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
else
    temp=/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp1_input
fi
if pgrep -x yambar >/dev/null; then
    while true; do
        sleep 1 &
        printf 'out|string|%s\n\n' "$(cut -c 1-2 "$temp")"
        wait
    done
else
    while true; do
        sleep 1 &
        cut -c 1-2 "$temp"
        wait
    done
fi
