#!/bin/sh
time1=$(date +%-S)
time2=$((60 - time1))
if pgrep -x yambar >/dev/null; then
    sleep $time2 &
    printf 'out|string|%s\n\n' "$(date "+%-I:%M%P")"
    wait
    while true; do
        sleep 60 &
        printf 'out|string|%s\n\n' "$(date "+%-I:%M%P")"
        wait
    done
else
    sleep $time2 &
    printf '%s\n' "$(date "+%-I:%M%P")"
    wait
    while true; do
        sleep 60 &
        printf '%s\n' "$(date "+%-I:%M%P")"
        wait
    done
fi
