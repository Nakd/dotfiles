#!/bin/sh
temp_min=45                                                 # min temp in C
temp_max=80                                                 # max temp in C
temp_range=$((temp_max - temp_min))                         # temp range
fan_min=25                                                  # min fan %
fan_max=100                                                 # max fan %
fan_range=$((fan_max - fan_min))                            # fan range
nvidia-settings -a "GPUFanControlState=1" > /dev/null 2>&1  # allow fan control
while true; do

    # current temp
    temp_cur=$(nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader)

    # temp delta above temp_min
    temp_delta=$((temp_cur - temp_min))

    # percent of the way to temp_max
    temp_perc=$(printf "%.0f\n" "$((1000000 * temp_delta / temp_range))e-4")

    # current fan speed
    fan_cur=$(nvidia-smi --query-gpu=fan.speed --format=csv,noheader | tr -d -- " %")

    # makes it possible to set fan_target to fan_min or fan_max
    if [ "$temp_perc" -le 0 ]; then
        fan_target=$fan_min
        if [ "$fan_cur" != "$fan_min" ]; then
            fan_check=-5
        else
            fan_check=0
        fi
    elif [ "$temp_perc" -ge 100 ]; then
        fan_target=$fan_max
        if [ "$fan_cur" != "$fan_max" ]; then
            fan_check=5
        else
            fan_check=0
        fi
    else

        # value that fan speed should be at
        fan_target=$(printf "%.0f\n" "$((temp_perc * fan_range / 100 + fan_min))e-0")

        # check if fan speed should change
        fan_check=$((fan_target - fan_cur))
    fi
        # echo "target: $fan_target"
    # printf "min temp            %sC\n" "$temp_min"
    # printf "max temp            %sC\n" "$temp_max"
    # printf "min fan             %s%%\n" "$fan_min"
    # printf "max fan             %s%%\n" "$fan_max"
    # printf "temp range          %s\n" "$temp_range"
    # printf "fan range           %s\n" "$fan_range"
    # printf "current temp        %sC\n" "$temp_cur"
    # printf "dist from min temp  %s\n" "$temp_delta"
    # printf "temp percent        %s\n" "$temp_perc"
    # printf "current fan         %s%%\n" "$fan_cur"
    # printf "target fan speed    %s%%\n" "$fan_target"

    # increase fan speed if necessary
    if [ "$fan_check" -ge 5 ]; then
        # printf "fan increase (attempt) at "
        # date "+%H:%M:%S"
        nvidia-settings -a "GPUTargetFanSpeed=$fan_target" > /dev/null 2>&1
        # printf "\033[92mfan speed changed from %s\n\033[0m" "$fan_cur% to $fan_target%"
        sleep 5

    # decrease fan speed if necessary
    elif [ "$fan_check" -le -5 ]; then
        # printf "fan decrease (attempt) at "
        # date "+%H:%M:%S"
        nvidia-settings -a "GPUTargetFanSpeed=$fan_target" > /dev/null 2>&1
        # printf "\033[34mfan speed changed from %s\n\033[0m" "$fan_cur% to $fan_target%"
        sleep 5
    fi
    sleep 5
done
