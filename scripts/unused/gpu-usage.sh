#!/bin/sh
while true; do
    sleep 1 &
    gpu_usage=$(nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader | head -c-3)
    case ${#gpu_usage} in
        1) printf '  %s%%\n' "$gpu_usage";;
        2) printf ' %s%%\n' "$gpu_usage";;
        3) printf '%s%%\n' "$gpu_usage";;
        *)
            printf '  ?%%\n'
            sleep 20 &
            ;;
    esac
    wait
done
