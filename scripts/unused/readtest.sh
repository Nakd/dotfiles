#!/bin/sh
set -e
trap "rm -f .fifo && killall $(basename "$0")" INT EXIT
mkfifo .fifo
bspc subscribe node_state | while read -r _ _ _ _ state status; do
    if [ "$state" = fullscreen ]; then
        echo "$status" > .fifo &
        # read -r line < .fifo
        # echo "$line"
    fi
done &

while true; do
    read -r line < .fifo
    echo "$line"
    sleep 1
done
