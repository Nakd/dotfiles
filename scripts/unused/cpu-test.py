#!/bin/python
from basic import *

try:
    arg[1]
except:
    # print('at least one argument is required')
    exit(2)

arg = arg[1:]
names = []
for i in arg:
    names += [i]
# print(names)
pids = []
for i in names:
    pid = cmd('pgrep -x ' + i)[:-1]
    if len(pid) > 0:
        pids += [pid]
        # print(pid)
    else:
        print(f'\033[34m{i}\033[0m not found')

if len(pids) == 0:
    exit(1)
# for i in pids:
print(pids)
# for i in $pid; do
#     total=$(awk '{print $14 + $15 + $16 + $17}' /proc/$i/stat)
#     printf "%s\n" "$total"
#     # p_uptime=$(awk '{print $22 / 100}' /proc/$i/stat)
#     # printf "%s\n" "$p_uptime"
# done
