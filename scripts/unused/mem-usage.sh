#!/bin/sh
if pgrep -x yambar >/dev/null; then
    while true; do
        sleep 5 &

        # get current mem usage
        mem=$(free -m | awk '/Mem/ {print $3 / $2}')

        # print 1 char if mem < 10%
        mem=$(printf %s "$mem" | cut -c 3-4)
        if [ "$mem" -lt 10 ]; then
            mem=$(printf %s "$mem" | cut -c 2)
        fi

        # print output
        case ${#mem} in
            1) printf 'out|string|  %s%%\n\n' "$mem";;
            2) printf 'out|string| %s%%\n\n' "$mem";;
            3) printf 'out|string|!!%s!!%%\n\n' "$mem";;
            *) printf 'out|string|mem usage error\n\n'
        esac
        wait
    done
else
    while true; do
        sleep 5 &

        # get current mem usage
        mem=$(free -m | awk '/Mem/ {print $3 / $2}')

        # print 1 char if mem < 10%
        mem=$(printf %s "$mem" | cut -c 3-4)
        if [ "$mem" -lt 10 ]; then
            mem=$(printf %s "$mem" | cut -c 2)
        fi

        # print output
        case ${#mem} in
            1) printf '  %s%%\n' "$mem";;
            2) printf ' %s%%\n' "$mem";;
            3) printf '!!!%s%%!!!\n' "$mem";;
            *) printf 'mem usage error\n'
        esac
        wait
    done
fi
