#!/bin/sh
IFS='
'
app_l=$(fd -uue desktop --max-depth 1 --strip-cwd-prefix --base-directory ~/.local/share/applications .)
app_u=$(fd -uue desktop --max-depth 1 --strip-cwd-prefix --base-directory /usr/share/applications .)
apps="${app_l}${app_u}"
for i in $apps; do
    applist="$applist${i%????????}\n"
done
result=$(printf "%b" "$applist" | sort -u | fzf)
gtk-launch "$result" "$f" 2>/dev/null
exit
if [ -n "$result" ]; then
    loc1="$HOME/.local/share/applications/$result.desktop"
    loc2="/usr/share/applications/$result.desktop"
    if [ -e "$loc1" ]; then
        cmd=$(perl -ne 'print for /^Exec.*/g' "$loc1")
    else
        cmd=$(perl -ne 'print for /^Exec.*/g' "$loc2")
    fi
    cmd=$(printf "%s" "$cmd" | perl -npe 's/ %[A-Za-z]//g')
    cmd=${cmd#?????}
    eval "$cmd" "$f"
    printf "\014"
fi
