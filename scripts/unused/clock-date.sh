#!/bin/sh
  cur_hour=$(date +%-H)
sleep_hour=$((24 - cur_hour))
 sleep_min=$((sleep_hour * 60))
   cur_min=$(date +%-M)
 sleep_min=$((sleep_min - cur_min))
   cur_sec=$(date +%-S)
 sleep_sec=$((sleep_min * 60))
 sleep_sec=$((sleep_sec - cur_sec))

if pgrep -x yambar >/dev/null; then
    sleep $sleep_sec &
    printf 'out|string|%s\n\n' "$(date "+%a %b %-m/%-d")"
    wait
    while true; do
        sleep 86400 &
        printf 'out|string|%s\n\n' "$(date "+%a %b %-m/%-d")"
        wait
    done
else
    sleep $sleep_sec &
    printf '%s\n' "$(date "+%a %b %-m/%-d")"
    wait
    while true; do
        sleep 86400 &
        printf '%s\n' "$(date "+%a %b %-m/%-d")"
        wait
    done
fi
